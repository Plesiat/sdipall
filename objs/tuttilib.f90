
	subroutine colig(nom,ilig,icol,op)

	implicit none

	integer ok
	integer icol, ilig, i, j, nc, test, nl

	parameter (nc=100000000)

	double precision, dimension (:), allocatable :: datl, datc
	double precision d_tmp

	character*(*) nom,op
	character*(LEN(trim(nom))) ntmp

9000	format (a,$)

	ilig=0
	icol=0
	test=0

	open(unit = 20, file = nom,status = 'old',iostat = ok)

	if(ok /= 0) then
	write(*,*)
	write(*,*) "Error!!! - Impossible to open the file"
	write(*,*) "Check that the file ", char(34),trim(nom),char(34), " is in the current folder"
	write(*,*)
	stop
	end if

	do while(ok == 0) !boucle pour compter le nombre de lig du fichier
	read(20,*,iostat = ok)
	if (ok == 0) ilig = ilig + 1
	end do

	rewind(20)

	allocate (datl(ilig))

	do i=1, ilig

	read(20,*) datl(i)

	enddo

	rewind(20)
        
        allocate(datc(nc))
        
	if ( ilig == 1 ) then
	
            do i=1,nc
            read(20,*,iostat = ok) datc(1:i)
            rewind(20)
            if (ok == -1) then
              test = 1
              icol = i-1
              exit
            endif
            enddo
            
	else 
            
            nl = int(ilig/4)
            if ( nl < 4 ) nl=ilig
            read(20,*,iostat = ok) (datc(i), i=1,nc)

            do j=2,nc

            if (datc(j)==datl(2) .and. datc(j*2-1)==datl(3)  .and. datc((j-1)*(nl-1)+1)==datl(nl)) then

            icol=j-1
            test=1
            exit

            endif

            enddo
        
        endif
        
        close(20)

	deallocate(datc)
	deallocate(datl)

	if (test==0) then

	write(*,*)
	write(*,*) "Error!!! - Impossible to count the number of columns"
	write(*,*) "Check that the file ", char(34), trim(nom), char(34), " has a homogeneous number of columns"
	write(*,*)

	stop

	endif


	if (op=="show") then

	do i=1,LEN(trim(nom))-1
	ntmp(i:i+1)=" "
	enddo

	write(*,*)
	write(*,'(a,a,a,i7)') trim(nom), "  >  ", "| nb lig :", ilig
	write(*,'(a,a,a,i7)') ntmp, "     ", "| nb col :", icol
	write(*,*) "____________________________________________________"

	endif


	return
	end

	subroutine nbtocar_z(nb,car,nd)

	implicit none

	integer nb,m,i,k, nd

	character*(*) car

	m=1

	do while (int(nb/(10**m)) .ne. 0)
		m=m+1
	enddo

	m=m-1

	car=char(48+nb-int((nb/10))*10)


	do i=1,m

	k=int(nb/(10**i))-int(nb/(10**(i+1)))*10
	car=char(48+k)//trim(car)

	enddo

	do i=m+2,nd,1
	car=char(48)//trim(car)
	enddo


	return
	end
	
	
	character*5 function int2str(num,nd)

	implicit none

	integer num,m,i,k, nd
	character*5 car

	m=1

	do while (int(num/(10**m)) .ne. 0)
		m=m+1
	enddo

	m=m-1

	car=char(48+num-int((num/10))*10)


	do i=1,m

	k=int(num/(10**i))-int(num/(10**(i+1)))*10
	car=char(48+k)//trim(car)

	enddo

	do i=m+2,nd,1
	car=char(48)//trim(car)
	enddo
    
    int2str = trim(car)

	return
	end

	
        subroutine glpar(xmin, xmax, glxi, nbint, ngl, np, ip, glxs, glst)
        
        implicit none
        
        integer, intent(in) :: nbint, nGL, np(2), ip
        double precision, intent(in) :: xmin, xmax, glxi(ngl)
        double precision, intent(out) :: glst
        double precision, intent(out) :: glxs(np(ip))
        
        integer i, j, k, nt
        double precision a, b, x(nbint*ngl)
        
        nt = nbint*ngl
        glst = (xmax-xmin)/(2.d0*dble(nbint))
        
        k=0
        do i=1, nbint
          a=xmin+dble(i-1)*2.d0*glst
          b=xmin+dble(i)*2.d0*glst

          do j=1,nGL
            k=k+1
            x(k) = glst*glxi(j)+(a+b)/2.d0
          enddo
          
        enddo
        
        if ( ip == 1 ) then
            k = 0
            do i=1, np(1)
              do j=1, np(2)
                k=k+1
                glxs(k) = x(i)
              enddo
            enddo
          else
            if ( ip == 2 ) then
              k = 0
              do i=1, np(1)
                do j=1, np(2)
                  k=k+1
                  glxs(k) = x(j)
                enddo
              enddo
            endif
          endif
        
        end subroutine glpar
        
        double precision function glint(glws, glst, func, nbint, ngl)
        
        implicit none
        
        integer, intent(in) :: nbint, nGL
        double precision, intent(in) :: glws(ngl), glst
        double precision, intent(in) :: func(ngl*nbint)
        
        integer i, j, k
        
        glint = 0.d0
        k=0
        do i=1, nbint
          do j=1,nGL
            k=k+1
            glint=glint+glst*func(k)*glws(j)
          enddo
        enddo
        
        return
        
        end function glint
        
        
        subroutine fullint(doigl,glst,nbint,ngl,glws,xtab,ytab,nx,I)
        
        implicit none
        
        logical, intent(in) :: doigl
        integer, intent(in) :: nbint, nGL
        double precision, intent(in) :: glws(ngl), glst
        integer, intent(in) :: nx
        double precision, intent(in) :: xtab(nx), ytab(nx)
        double precision, intent(out) :: I
        
        double precision glint
        
        if ( nx == 1 ) then
          I = ytab(1)
        else
          if ( nbint == 0 ) then
             I = sum(ytab)*(xtab(2)-xtab(1))
          else
          if (doigl) then
            I = glint(glws, glst, ytab, nbint, nGL)
          else
            call intGL(xtab, ytab, nx, xtab(1), xtab(nx), I, nbint, nGL)
          endif
          endif
        endif
        
        return
        end subroutine fullint

        subroutine wrapint(nbint,nGL,xtab,ytab,nx,I)
        
        integer, intent(in) :: nx, nbint, nGL
        double precision, intent(in) :: xtab(nx), ytab(nx)
        double precision, intent(out) :: I
        
        if ( nx == 1 ) then
          I = ytab(1)
        else
          call intGL(xtab,ytab,nx,xtab(1),xtab(nx),I,nbint,nGL)
        endif
        
        return  
        
        end subroutine wrapint
        
!ccccccccccccccccccccccccccccccccccccccccccccccccc INFO ccccccccccccccccccccccccccccccccccccccccccccccccc!
! - xmin, xmax : limites d'integration
! - It : valeur de l'intégrale (renvoyée à la fin)
! - nbint et nGL : paramètres propres à l'integration de Gauss-Legendre
!		- nbint = nb de subintervales voulu dans l'interval d'intégration considéré
!		- nGL = ordre de Gauss-Legendre
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc!

	subroutine intGL(xtab,ytab,lig,xmin,xmax,It,nbint,nGL)

	implicit none

        integer, intent(in) :: lig, nbint, nGL
        double precision, intent(in) :: xtab(lig), ytab(lig)
        double precision, intent(in) :: xmin, xmax
        double precision, intent(out) :: It


	integer i, j
	double precision a, b, w(nGL), x(nGL), xR

	integer nmax
	parameter(nmax=10000)
	double precision, dimension (:), allocatable ::  xx, yy, yd2
	double precision ydi, ydf, res

	allocate(xx(nmax))
	allocate(yy(nmax))
	allocate(yd2(nmax))

	do i=1,lig
            if ( i > 1 ) then
                if ( xtab(i) <= xtab(i-1) ) then
                    write(*,*) "Error in intGL! The grid is not sorted correctly!"
                    write(*,*) "i, xtab(i), xtab(i-1)=", i, xtab(i), xtab(i-1)
                    stop
                endif
            endif
            xx(i)=xtab(i)
            yy(i)=ytab(i)
	enddo

	ydi=(yy(2)-yy(1))/(xx(2)-xx(1))
	ydf=(yy(lig)-yy(lig-1))/(xx(lig)-xx(lig-1))

	call spline(xx,yy,lig,ydi,ydf,yd2)
	call gaussweight(x,w,nGL)

	It=0.d0
	do i=1,nbint

            a=xmin+dble(i-1)*(xmax-xmin)/(dble(nbint))
            b=xmin+dble(i)*(xmax-xmin)/(dble(nbint))

            do j=1,nGL
                xR=((b-a)/2.d0)*x(j)+(a+b)/2.d0

		 if ((xR.gt.xx(lig)).or.(xR.lt.xx(1))) then
			write(*,*) 'Prob0: coef', 'x,xx(1),xx(nf)',xR,xx(1),xx(lig), xmin, xmax, a, b
			stop
		 endif
                call splint(xx,yy,yd2,lig,xR,res)

		It=It+((b-a)/2.d0)*res*w(j)
            enddo

	enddo

	deallocate(xx)
	deallocate(yy)
	deallocate(yd2)

	return

	end subroutine intGL

        subroutine gaussweightx(xmin,xmax,nGL1,nGL2,xint,w)

            implicit none

            integer, intent(in) :: nGL1, nGL2
            double precision, intent(in) :: xmin, xmax
            double precision, intent(out) :: xint(nGL1*nGL2), w(nGL2)

            integer i, j, k
            double precision a, b, x(nGL2)


            call gaussweight(x,w,nGL2)
            k = 1
            do i=1,nGL1
                a=xmin+dble(i-1)*(xmax-xmin)/(dble(nGL1))
                b=xmin+dble(i)*(xmax-xmin)/(dble(nGL1))
                do j=1,nGL2
                    xint(k)=((b-a)/2.d0)*x(j)+(a+b)/2.d0
                    k = k+1
                enddo
            enddo

        return
        end subroutine gaussweightx

        subroutine intgaussweight(xmin,xmax,nGL1,nGL2,y,w,it)

            implicit none

            integer, intent(in) :: nGL1, nGL2
            double precision, intent(in) :: xmin, xmax
            double precision, intent(in) :: y(nGL1*nGL2), w(nGL2)
            double precision, intent(out) :: it

            integer i, j, k
            double precision a, b

            it=0.d0
            k = 1
            do i=1,nGL1
                a=xmin+dble(i-1)*(xmax-xmin)/(dble(nGL1))
                b=xmin+dble(i)*(xmax-xmin)/(dble(nGL1))
                do j=1,nGL2
                    it = it+((b-a)/2.d0)*y(k)*w(j)
                    k = k+1
                enddo
            enddo

        return
        end subroutine intgaussweight

!c@------------------------- Subroutine de calcul des poids de Gauss-Legendre



      SUBROUTINE gaussweight(X,W,N)

      implicit none
      integer I, J, M, N
      double precision Z, P1, P2, P3, EPS, ONE, TWO, HAF, QUA, PI
      double precision PP, Z1
      double precision  X(N),W(N)
      PARAMETER (EPS=1.D-15)
      PARAMETER (ONE=1.d0, TWO=2.d0, HAF=one/two, QUA=haf/two)
      PARAMETER (PI=2.d0*dasin(1.d0))
      M=(N+1)/2
      DO 12 I=1,M
        Z=DCOS(PI*(I-QUA)/(N+HAF))
1       CONTINUE
          P1=ONE
          P2=0.d0
          DO 11 J=1,N
            P3=P2
            P2=P1
            P1=((TWO*J-ONE)*Z*P2-(J-ONE)*P3)/J
11        CONTINUE
          PP=N*(Z*P1-P2)/(Z*Z-ONE)
          Z1=Z
          Z=Z1-P1/PP
        IF(DABS(Z-Z1).GT.EPS)GO TO 1
        X(I)=-Z
        X(N+1-I)=Z
        W(I)=TWO/((ONE-Z*Z)*PP*PP)
        W(N+1-I)=W(I)
12    CONTINUE
      RETURN
      END


      subroutine interpogrid2(xx,yy,np,grid,ynew,npmax)

      implicit none
      integer i, np, npmax
      double precision xx(np), yy(np), grid(npmax), ynew(npmax)
      double precision xmin, xmax

      double precision  yd2(np)
      double precision ydi, ydf
      
      if ( np == 1 ) then
        do i=1,npmax
          ynew(i) = yy(1)
        enddo
      else

      xmin=grid(1)
      xmax=grid(npmax)

      ydi=(yy(2)-yy(1))/(xx(2)-xx(1))
      ydf=(yy(np)-yy(np-1))/(xx(np)-xx(np-1))

      call spline(xx,yy,np,ydi,ydf,yd2)

      do i=1,npmax
	      if ((grid(i).gt.xx(np)).or.(grid(i).lt.xx(1))) then
		write(*,*) 'Prob0: coef', 'x,xx(1),xx(nf)',grid(i),xx(1),xx(np)
		stop
	      endif
              call splint(xx,yy,yd2,np,grid(i),ynew(i))
      enddo
      
      endif

      return
      end
      
    double precision function interpo4(xtmp,ytmp,np,Ei)

	implicit none
      integer i, j, np, nt
      double precision xtmp(np),ytmp(np), Ei

      integer nmax
      parameter(nmax=100000)
      double precision, dimension (:), allocatable ::  xx,yy,yd2
      double precision x, ydi, ydf, res

	allocate(xx(nmax))
	allocate(yy(nmax))
	allocate(yd2(nmax))
    
      j = 0
      do i=1,np
        if ( xtmp(i) == Ei ) cycle
        j = j+1
        xx(j)=xtmp(i)
        yy(j)=ytmp(i)
      enddo
      nt = j

       if (xx(2)==xx(1)) then
		write(*,*) "Error!!! Repeated initial point!"
		stop
      endif
      if (xx(nt)==xx(nt-1)) then
		write(*,*) "Error!!! Repeated final point!"
		stop
      endif

      ydi=(yy(2)-yy(1))/(xx(2)-xx(1))
      ydf=(yy(nt)-yy(nt-1))/(xx(nt)-xx(nt-1))

      call spline(xx,yy,nt,ydi,ydf,yd2)

      x=Ei

	      if ((x.gt.xx(nt)).or.(x.lt.xx(1))) then
		write(*,*) 'Prob0: coef', 'x,xx(1),xx(nf)',x,xx(1),xx(nt)
		stop
	      endif
      call splint(xx,yy,yd2,nt,x,res)

      interpo4=res


      deallocate(xx)
      deallocate(yy)
      deallocate(yd2)

      return
      end


	SUBROUTINE spline(x,y,n,yp1,ypn,y2)
	implicit none
	INTEGER n
	double precision yp1,ypn,x(n),y(n),y2(n)
	integer nmax
	parameter(nmax=100000)
	INTEGER i,k
	double precision p,qn,sig,un

	double precision, dimension (:), allocatable ::  u
	allocate(u(nmax))

	if (yp1.gt..99e30) then
		y2(1)=0.
		u(1)=0.
	else
		y2(1)=-0.5
		u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
	endif

	do i=2,n-1
		sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
		p=sig*y2(i-1)+2.
		y2(i)=(sig-1.)/p
		u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
	enddo

	if (ypn.gt..99e30) then
		qn=0.
		un=0.
	else
		qn=0.5
		un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
	endif

	y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)

	do k=n-1,1,-1
		y2(k)=y2(k)*y2(k+1)+u(k)
	enddo

	deallocate(u)

	return
	END

!                    ------------------------                    !

	SUBROUTINE splint(xa,ya,y2a,n,x,y)
	implicit none
	INTEGER n
	double precision x,y,xa(n),y2a(n),ya(n)
	INTEGER k,khi,klo
	double precision a,b,h
	klo=1
	khi=n

1	if (khi-klo.gt.1) then
		k=(khi+klo)/2
		if(xa(k).gt.x)then
			khi=k
		else
			klo=k
		endif
	goto 1
	endif

	h=xa(khi)-xa(klo)
	if (h.eq.0.) then
		write(*,*) "bad caca input in splint"
		stop
	endif
	a=(xa(khi)-x)/h
	b=(x-xa(klo))/h
	y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
	return
	END


!                    ------------------------                    !

      double precision function coef(x)
      implicit none
      integer nmax, np, nf
      parameter(nmax=100000)
      double precision x,xx(nmax),yy(nmax),yd2(nmax)
      common/splines/xx,yy,yd2,nf

      double precision res

      np=nf

      if ((x.gt.xx(np)).or.(x.lt.xx(1))) then
        write(6,*) 'Prob0: coef', 'x,xx(1),xx(nf)',x,xx(1),xx(np)
        stop
      endif
      call splint(xx,yy,yd2,np,x,res)
      coef=res
      return
      end


!                    ------------------------                    !


      subroutine precubsp(xtmp,ytmp,np)
      implicit none
      integer i, nmax, np, nf
      parameter(nmax=100000)
      double precision xtmp(nmax),ytmp(nmax), ydi, ydf
      double precision x,xx(nmax),yy(nmax),yd2(nmax)
      common/splines/xx,yy,yd2,nf

      nf=np

      do i=1,np
        xx(i)=xtmp(i)
        yy(i)=ytmp(i)
      enddo

!      Calcula la primera derivada en el primer y en el ultimo punto

      ydi=(yy(2)-yy(1))/(xx(2)-xx(1))
      ydf=(yy(np)-yy(np-1))/(xx(np)-xx(np-1))

!      Calcula las segundas derivadas

      call spline(xx,yy,nf,ydi,ydf,yd2)

      return
      end


        subroutine rebatan(php_old,php,dipr,dipi,pmod)

        implicit none

        double precision, intent(inout) :: php_old
        double precision, intent(in) :: dipr, dipi
        double precision, intent(out) :: php
        integer, intent(in) :: pmod
        !double precision, optional, intent(in) :: thd

        integer i, j, s
        double precision eps
        double precision div, dphp, mpig, dmin, dtmp
        double precision pig
        parameter (pig = 4.d0*datan(1.d0))
        logical dorec
!         logical pmod

!         pmod=.False.
!         if (present(dipr)) then
!           if (present(dipi)) then
!             pmod=.True.
!           else
!             write(*,*) "Error! When dipr argument is present, dipi must be present too!"
!           endif
!         endif

!         if (present(thd)) then
!           eps=thd
!         else
!           eps=1.d-18
!         endif
        eps=1.d-18
        dorec=.True.
!         dorec=.False.
        if ( pmod < 0 ) then
          php=datan2(dipi,dipr)
        else
            if ( pmod > 0 ) then
                if ( dabs(dipr) <= eps ) then
                    if ( dabs(dipi) <= eps ) then
                        php = 0.d0
                    else
                        if ( dipr > 0.d0 ) then
                            php = pig/2.d0
                        else
                            php = -pig/2.d0
                        endif
                    endif
                else
                    php = datan2(dipi,dipr)
                endif
                div = 1.d0
            else
                php = dipr
                div = dipi
            endif ! pmod

            dphp = php-php_old
            dmin = dphp
            i=1
            s=0
            do while ( i <= 200 )
                if ( s == 0 ) then
                    do j=-1, 1, 2
                        dtmp = dphp+j*i*pig/div
                        if ( dabs(dtmp) < dabs(dmin) ) then
                            dmin = dtmp
                            s = j
                        endif
                    enddo
                    if ( s == 0 ) exit
                else
                    dtmp = dphp+s*i*pig/div
                    if ( dabs(dtmp) < dabs(dmin) ) then
                        dmin = dtmp
                    else
                        exit
                    endif
                endif
                i=i+1
            enddo
            
            if ( i == 201 ) write(*,*) "Warning!! rebatan not converged!"
	
            php = dmin+php_old
        endif

        if ( pmod == 0 .or. pmod == 2 ) php_old = php

        return

        end subroutine rebatan

        
	integer function fact(n)

	implicit none
	integer i, n

	fact=1
	if (n .ne. 0) then
		do i=2, n
			fact=fact*i
		enddo
	endif

	return
	end


	double precision function d_fact(n)

	implicit none
	integer i, n

	d_fact=1.d0
	if (n .ne. 0) then
		do i=2, n
			d_fact=d_fact*dble(i)
		enddo
	endif

	return
	end

	
	

    
!$vecop: module vecop
!#vecop: This module contains functions and subroutines for basic vector operations: 
!#vecop: norm, cross product, orthogonal vector generator,
!#vecop: conversion of polar and azimuthal angles to normalized vector in cartesian coordinates
module vecop

implicit none

contains

!$normvec: double precision function normvec(u1)
!#normvec: Computes the norm of the vector u1(u1x,u1y,u1z)
double precision function normvec(u1)

  implicit none

  double precision, intent(in) :: u1(3)

  normvec=dsqrt(u1(1)**2+u1(2)**2+u1(3)**2)

  return

end function normvec

!$crossprod: function crossprod(u1,u2) - double precision
!#crossprod: Computes the cross product of the vectors u1(u1x,u1y,u1z) and u2(u2x,u2y,u2z)
function crossprod(u1,u2)

  implicit none

  double precision, intent(in) :: u1(3), u2(3)
  double precision crossprod(3)

  crossprod(1)=u1(2)*u2(3)-u1(3)*u2(2)
  crossprod(2)=u1(3)*u2(1)-u1(1)*u2(3)
  crossprod(3)=u1(1)*u2(2)-u1(2)*u2(1)

end function crossprod

!$genorthvec: function genorthvec(u1) - double precision
!&genorthvec: Depends on crossprod
!#genorthvec: Generates automatically an arbitrary orthogonal vector to u1(u1x,u1y,u1z)
function genorthvec(u1)

  implicit none

  double precision, intent(in) :: u1(3)
  double precision genorthvec(3)

  double precision u0(3), u2(3)

  u2=0.d0
  !% Let's take an arbitrary vector u2 to generate 
  !% an arbitrary orthognal vector u0 to the plane defined by u1, u2
  u2(1)=1.d0
  u0=crossprod(u1,u2)
  !% If by any chance u1 and u2 are collinear, let's take another arbitrary vector u2
  if ( normvec(u0) == 0.d0 ) then
    u2=0.d0
    u2(2)=1.d0
    u0=crossprod(u1,u2)
  endif
  genorthvec=u0

end function genorthvec

!$ang2vec: function ang2vec(theta,phi,amode) - double precision
!#ang2vec: Converts the polar (theta) and azimuthal (phi) angles 
!#ang2vec: to a normalized vector in cartesian coordinates.
!#ang2vec: amode (= "degree" or "radian") defines the unit of the given angles
function ang2vec(theta,phi,amode)

  implicit none

  double precision, intent(in) :: theta, phi
  character*(*), intent(in) :: amode
  double precision ang2vec(3)

  double precision theta2, phi2
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    theta2=theta*pig/180.d0
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      theta2=theta
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif

  ang2vec(1)=dsin(theta2)*dcos(phi2)
  ang2vec(2)=dsin(theta2)*dsin(phi2)
  ang2vec(3)=dcos(theta2)

end function ang2vec


!$vecrotperplan: function vecrotperplan(u0,u1,phi,amode) - double precision
!&vecrotperplan: Depends on crossprod, normvec
!#vecrotperplan: Calculates a vector that belongs to the plan perpendicular u0 and containing u1, and which is rotated by an angle phi (clockwise) with respect to u1
function vecrotperplan(u0,u1,phi,amode)
  
  implicit none
  
  double precision, intent(in) :: u0(3), u1(3)
  double precision, intent(in) :: phi
  character*(*), intent(in) :: amode
  double precision vecrotperplan(3)
  
  double precision phi2
  double precision u1b(3)
  double precision u2(3)
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif
  
  !% Calculate the second basis vector (perpendicular to u0)
  u2=crossprod(u0,u1)
  u2=u2/normvec(u2)
  u1b=u1/normvec(u1)
  
  !% Calculate the required vector in the normalized vector bases
  vecrotperplan=dcos(phi2)*u1b+dsin(phi2)*u2
  
  end function vecrotperplan
  
  
!$vecrotperplan2: function vecrotperplan(u0,u1,phi,amode) - double precision
!&vecrotperplan2: Depends on crossprod, normvec
!#vecrotperplan2: 
function vecrotperplan2(u0,u1,phi,amode)
  
  implicit none
  
  double precision, intent(in) :: u0(3), u1(3)
  double precision, intent(in) :: phi
  character*(*), intent(in) :: amode
  double precision vecrotperplan2(3)
  
  double precision phi2
  double precision u1b(3)
  double precision u2(3)
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree" ) then
    phi2=phi*pig/180.d0
  else
    if ( amode == "radian" ) then
      phi2=phi
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif
  
  !% Calculate the first basis vector (perpendicular to u0 and u1)
  u1b=crossprod(u0,u1)
  !% Calculate the second basis vector (perpendicular to u0 and u1b)
  u2=crossprod(u0,u1b)
  u1b=u1b/normvec(u1b)
  u2=u2/normvec(u2)
  
  !% Calculate the required vector in the normalized vector bases
  vecrotperplan2=dcos(phi2)*u1b+dsin(phi2)*u2
  
  end function vecrotperplan2
  

end module vecop


!$cart2sph: subroutine cart2sph(cmt,natm,amode,op)
!#cart2sph: Converts cartesian coordinates <-> spherical coordinates.
!#cart2sph: cmt is a 2-dimensional array containing the cartesian coordinates or the spherical coordinates
!#cart2sph: amode (= "degree" or "radian") defines the unit of the angles
!#cart2sph: op (=-1 or 1) defines the conversion direction: -1) cartesian coordinates -> spherical coordinates ; 1) spherical coordinates -> cartesian coordinates
!#cart2sph: The number of atoms (natm) must be given
subroutine cart2sph(cmt,natm,amode,op)

  implicit none

  integer, intent(in) :: natm, op
  character*(*), intent(in) :: amode
  double precision, intent(inout) :: cmt(natm,3)

  integer i, facang
  double precision xR, ytheta, zphi
  double precision pig
  parameter (pig = 4.d0*datan(1.d0))

  if ( amode == "degree") then
    facang=pig/180.d0
  else
    if ( amode == "radian" ) then
      facang=1.d0
    else
      write(*,*)
      write(*,*) "Error!!! Bad argument for option amode: amode=", amode
      write(*,*)
      stop
    endif
  endif

  select case(op)
  !% cartesian coordinates -> spherical coordinates
  case (1)
    do i=1, natm
      xR=cmt(i,1)
      ytheta=cmt(i,2)
      zphi=cmt(i,3)
      cmt(i,1)=dsqrt(xR**2+ytheta**2+zphi**2)
      if ( xR == 0.d0 ) then
        if ( ytheta == 0.d0 ) then
          cmt(i,3)=0.d0
        else
          cmt(i,3)=pig/2.d0
        endif
      else
	cmt(i,3)=datan(ytheta/xR)
	if ( xR < 0.d0 ) then
	  cmt(i,3)=cmt(i,3)+pig
	endif
      endif
      if ( cmt(i,1) == 0.d0 ) then
        cmt(i,2)=0.d0
      else
        cmt(i,2)=dacos(zphi/cmt(i,1))
      endif
      cmt(i,2:3)=cmt(i,2:3)*facang
    enddo
  
  !% spherical coordinates -> cartesian coordinates
  case(-1)
    do i=1, natm
      xR=cmt(i,1)
      ytheta=cmt(i,2)*facang
      zphi=cmt(i,3)*facang
      cmt(i,1)=xR*dsin(ytheta)*dcos(zphi)
      cmt(i,2)=xR*dsin(ytheta)*dsin(zphi)
      cmt(i,3)=xR*dcos(ytheta)
    enddo

  case DEFAULT
    write(*,*)
    write(*,*) "Error! Bad op option in cart2sph!"
    write(*,*)
    stop

  end select

  return

end subroutine cart2sph

!$rotmatgen: subroutine rotmatgen(RM,ind,ang)
!#rotmatgen: Generates the rotation matrix (RM) for the axes x, y or z defined by the index (ind=1, 2, 3 respectively) and for the angle ang (in radian)
subroutine rotmatgen(RM,ind,ang)

  implicit none

  integer, intent(in) :: ind
  double precision, intent(in) :: ang
  double precision, intent(out) :: RM(3,3)

  RM=0.d0
  select case (ind)

  case (1)
    RM(1,1)=1.d0
    RM(2,2)=dcos(ang)
    RM(2,3)=-dsin(ang)
    RM(3,2)=dsin(ang)
    RM(3,3)=dcos(ang)

  case(2)
    RM(1,1)=dcos(ang)
    RM(1,3)=dsin(ang)
    RM(2,2)=1.d0
    RM(3,1)=-dsin(ang)
    RM(3,3)=dcos(ang)

  case(3)
    RM(1,1)=dcos(ang)
    RM(1,2)=-dsin(ang)
    RM(2,1)=dsin(ang)
    RM(2,2)=dcos(ang)
    RM(3,3)=1.d0

  case default
    write(*,*)
    write(*,*) "Error! Bad option for rotation matrix."
    write(*,*)
    stop

  end select

  return

end subroutine rotmatgen

!$rotmatavec: subroutine rotmatavec(cmt,natm,u1,u2)
!&rotmatavec: Depends on ramatgen
!#rotmatavec: Rotates the cartesian coordinates contained in the 2-dimensional array (cmt) by the angles needed to align the vector u1 with the vector u2
!#rotmatavec: The number of atoms (natm) must be given
subroutine rotmatavec(cmt,natm,u1,u2)

  use vecop

  implicit none

  integer, intent(in) :: natm
  double precision, intent(in) :: u1(3), u2(3)
  double precision, intent(inout) ::  cmt(natm,3)

  integer i, j, k
  double precision u1b(3), u2b(3), u2c(3), u3(3)
  double precision RM(3,3), xyz1(3), xyz2(3)
  logical testvec

  u1b=u1/normvec(u1)
  u2b=u2/normvec(u2)
  
  !% Computes the rotation matrix RM defined by the normalized vectors u1b and u2b
  call ramatgen(u1b,u2b,RM)

  do i=1, natm
    do j=1, 3
      xyz1(j)=cmt(i,j)
    enddo
    !% Rotates each row of the array of cartesian coordinates cmt
    xyz2=matmul(RM,xyz1)
    do j=1, 3
      cmt(i,j)=xyz2(j)
    enddo
  enddo

  return

end subroutine rotmatavec


!$ramatgen: subroutine ramatgen(u1,u2,RM)
!&ramatgen: Depends on normvec, crossprod
!#ramatgen: Computes the rotation matrix RM needed to align the vector u1 with the vector u2
subroutine ramatgen(u1,u2,RM)

  use vecop

  implicit none

  double precision, intent(in) :: u1(3), u2(3)
  double precision, intent(out) :: RM(3,3)

  integer i, j
  double precision sang, cang, fang
  double precision IM(3,3), u3(3), vx(3,3)

  IM=0.d0
  do i=1, 3
    IM(i,i)=1.d0
  enddo

  u3=crossprod(u1,u2)
  sang=normvec(u3)

  if ( sang == 0.d0 ) then
    !% Treats the specific case where u1 and u2 are collinears
    cang=dot_product(u1,u2)
    fang=dot_product(u1,u1)
    !% If u1 and u2 are collinear, the rotation matrix is equal to unity, If they are anticollinear, we have a problem.
    if ( cang == fang ) then
      RM=IM
    else
      write(*,*)
      write(*,*) "Error!!! Problem with anticollinear vectors!"
      write(*,*)
      stop
    endif

  else
    !% If u1 and u2 are not collinears, compute the rotation matrix in the usual way
    cang=dot_product(u1,u2)
    fang=(1.d0-cang)/(sang**2)

    vx=0.d0
    vx(1,2)=-u3(3)
    vx(1,3)=u3(2)
    vx(2,1)=u3(3)
    vx(2,3)=-u3(1)
    vx(3,1)=-u3(2)
    vx(3,2)=u3(1)

    RM=IM+vx+matmul(vx,vx)*fang
  endif

  return

end subroutine ramatgen


subroutine remtime(i,ni,initime,crate,title,rank)
  implicit none
  
  integer, intent(in) :: i, ni
  integer, intent(inout) :: initime, crate
  character*(*) title
  integer, optional :: rank
  
  integer curtime, nmod
  double precision caltime(2)
  integer j, id, hr, mn, sc
  character*12 rankinf
  character*50 strtime(2)
  
  id=0
  nmod = 2
  if ( ni < 1 ) nmod = 1
  
  rankinf = ""
  if (present(rank)) then
    if ( ni == -1 ) then
      write(rankinf,'(a,i4)') " / rank ", rank
    else
      id=rank
    endif
  endif
  
  if ( id == 0 ) then
    strtime(1)=" o "//trim(title)//" | Etime: "
    strtime(2)=" / Rtime: "
  
!     call CPU_TIME(curtime)
    if ( i == 1 ) then
        call system_clock(count_rate=crate)
        call system_clock(initime)
!         write(*,*)
!         print '(a,$)', " o Running time evaluation initialized..."
    else
    call system_clock(curtime)
    caltime(1)=(curtime-initime)/crate
    caltime(2)=caltime(1)*(ni-i+1)/dble(i-1)
    
    print '(A,$)',char(27)//"[1A"
    do j=1, nmod
        sc=floor(mod(caltime(j),60.d0))
        mn=floor(mod(caltime(j)/60,60.d0))
        hr=floor(caltime(j)/3600)
        print '(a,i4,a,i2,a,i2,a,$)', &
        trim(strtime(j)), hr, "h ", mn, "mn ", sc, "s "//trim(rankinf)
    enddo
    write(*,*)
    endif
  endif
  
end subroutine remtime

subroutine sliceprocs(my_id,num_procs,ngrid,iien)
  implicit none
  
  integer, intent(in) :: my_id, num_procs, ngrid
  integer, intent(out) :: iien(0:num_procs-1,3)
  
  integer i, n, nrg
  
  n = 0
  do i=0, num_procs-1
    nrg = int(ngrid/num_procs)
    if ( i < mod(ngrid,num_procs) ) nrg = nrg+1
    iien(i,1) = n+1
    n = n+nrg
    iien(i,2) = n
    iien(i,3) = iien(i,2)-iien(i,1)+1
  enddo
  
  return
  
end subroutine sliceprocs

subroutine cart2sph_(xyz)

  implicit none

  double precision, intent(inout) :: xyz(3)
  double precision rtp(3)

  double precision pig
  parameter(pig=4.d0*datan(1.d0))

  rtp(1) = dsqrt(xyz(1)**2 + xyz(2)**2 + xyz(3)**2)
  if (xyz(1) == 0.d0) then
    if (xyz(2) == 0.d0) then
      rtp(3) = 0.d0
    else
      rtp(3) = pig/2.d0
    end if
  else
    rtp(3) = datan(xyz(2)/xyz(1))
    if (xyz(1) < 0.d0) then
      rtp(3) = rtp(3) + pig
    end if
  end if
  if (rtp(1) == 0.d0) then
    rtp(2) = 0.d0
  else
    rtp(2) = dacos(xyz(3)/rtp(1))
  end if
  if (rtp(3) < 0.d0) rtp(3) = 2.d0*pig + rtp(3)
    
  xyz = rtp
  return

end subroutine cart2sph_

     subroutine interp1d(xx,yy,np,xnew,ynew,ni,imode)
     
      implicit none
      integer, intent(in) :: np, ni
      double precision, intent(in) :: xx(np), yy(np), xnew(ni)
      double precision, intent(out) :: ynew(ni)
      character*(*) imode
      
      select case(trim(imode))
      case("linear")
        call interpolin(xx,yy,np,xnew,ynew,ni)
      case("cubic")
        call interpogrid2(xx,yy,np,xnew,ynew,ni)
      case DEFAULT
        write(*,*) "Error! Bad mode interp1d!"
        stop

     end select
     
     end subroutine interp1d
      

     subroutine interpolin(xx,yy,np,xnew,ynew,ni)

      implicit none
      integer, intent(in) :: np, ni
      double precision, intent(in) :: xx(np), yy(np), xnew(ni)
      double precision, intent(out) :: ynew(ni)
      
      integer i, i0, i1
      double precision stp
      
      stp = (xx(np)-xx(1))/(np-1)
      
      do i=1, ni
        i0 = int((xnew(i)-xx(1))/stp)+1
        i1 = i0+1
        ynew(i) = (yy(i0)*(xx(i1)-xnew(i))+yy(i1)*(xnew(i)-xx(i0)))/(xx(i1)-xx(i0))
      enddo

      return
      end subroutine interpolin

      
     subroutine getgrid(nseq,gpar,grid)

      implicit none

      integer, intent(in) :: nseq
      double precision, intent(in) :: gpar(nseq,3)
      double precision, intent(out) :: grid(int(sum(gpar(:,1))))

      integer i, j, k, np(2)
      double precision stp
      
      k = 0
      do i=1, nseq
        if ( gpar(i,1) == 1.d0 ) then
          stp = 0.d0
        else
          stp = (gpar(i,3) - gpar(i,2))/(gpar(i,1) - 1.d0)
        endif
        do j=1, int(gpar(i,1))
          k = k+1
          grid(k) = gpar(i,2) + dble(j - 1)*stp
        enddo
      enddo


    end subroutine getgrid

    
    subroutine splinex(x,y,y2,n)
        
        implicit none
        integer, intent(in) :: n
        double precision, intent(in) :: x(n), y(n)
        double precision, intent(out) :: y2(n)

        integer i, k
        double precision yp1, ypn
        double precision p, qn, sig, un
        double precision u(n)
        
        if ( n == 1 ) then
          y2 = 0.d0
        else
        
        yp1=(y(2)-y(1))/(x(2)-x(1))
        ypn=(y(n)-y(n-1))/(x(n)-x(n-1))
        
        if (yp1.gt..99e30) then
                y2(1)=0.
                u(1)=0.
        else
                y2(1)=-0.5
                u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
        endif
        
        do i=2,n-1
                sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
                p=sig*y2(i-1)+2.
                y2(i)=(sig-1.)/p
                u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
        enddo
        
        if (ypn.gt..99e30) then
                qn=0.
                un=0.
        else
                qn=0.5
                un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
        endif
        
        y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)
        do k=n-1,1,-1
                y2(k)=y2(k)*y2(k+1)+u(k)
        enddo
        
        endif
        
        return
        
    end subroutine splinex
        

    double precision function splintx(xa,ya,y2a,n,x)
        
        implicit none
        integer, intent(in) :: n
        double precision, intent(in) :: x, xa(n), y2a(n), ya(n)
        
        integer k, khi, klo
        double precision a, b, h
        
        if ( n == 1 ) then
        
          splintx = ya(1)
          
        else

        klo=1
        khi=n
        
1        if (khi-klo.gt.1) then
                k=(khi+klo)/2
                if(xa(k).gt.x)then
                        khi=k
                else
                        klo=k
                endif
        goto 1
        endif
        
        h=xa(khi)-xa(klo)
        if (h.eq.0.) then
                write(*,*) "Error! Bad grid input in splint!" 
                stop
        endif
        
        a=(xa(khi)-x)/h
        b=(x-xa(klo))/h
        splintx=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
        
        endif
        
        return
        
    end function splintx

    subroutine gradinplace(xx,yy,np,eps)
     
      implicit none
      integer, intent(in) :: np
      double precision, intent(in) :: xx(np), eps
      double precision, intent(inout) :: yy(np)
      
      double precision ynew(np)
      
    if ( eps == 0.d0 ) then
      call central_diff(xx,yy,np,ynew)
    else
        call interpodiff(xx,yy,np,ynew,eps)
    endif
    
    yy = ynew
     
    end subroutine gradinplace
    
    subroutine gradient(xx,yy,np,ynew,eps)
     
      implicit none
      integer, intent(in) :: np
      double precision, intent(in) :: xx(np), yy(np), eps
      double precision, intent(out) :: ynew(np)
    
    if ( eps <= 0.d0 ) then
      call central_diff(xx,yy,np,ynew)
    else
        call interpodiff(xx,yy,np,ynew,eps)
    endif
     
    end subroutine gradient
    
    subroutine central_diff(xx,yy,np,ynew)
    
      implicit none
      integer, intent(in) :: np
      double precision, intent(in) :: xx(np), yy(np)
      double precision, intent(out) :: ynew(np)
      
      integer i
      
      ynew(1) = (yy(2)-yy(1))/(xx(2)-xx(1))
      ynew(np) = (yy(np)-yy(np-1))/(xx(np)-xx(np-1))
      
      do i=2, np-1
        ynew(i) = (yy(i+1)-yy(i-1))/(xx(i+1)-xx(i-1))
      enddo
    
    end subroutine central_diff

    
    subroutine interpodiff(xx,yy,np,ynew,eps)
    
      implicit none
      integer, intent(in) :: np
      double precision, intent(in) :: xx(np), yy(np), eps
      double precision, intent(out) :: ynew(np)
      
      integer i
      double precision y2(np), splintx, deps
      
      deps = 2.d0*eps
      
      call splinex(xx,yy,y2,np)
      
      
      ynew(1) = (splintx(xx,yy,y2,np,xx(1)+eps)-yy(1))/eps
      ynew(np) = (yy(np)-splintx(xx,yy,y2,np,xx(np)-eps))/eps
      
      do i=2, np-1
        ynew(i) = (splintx(xx,yy,y2,np,xx(i)+eps)-splintx(xx,yy,y2,np,xx(i)-eps))/deps
      enddo
    
    end subroutine interpodiff
