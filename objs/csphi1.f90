

!     -----------------------------------------------------------------
      subroutine csphi1(l, cp, sp, q) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
!     -----------------------------------------------------------------
!...Translated by Pacific-Sierra Research 77to90  4.3C  15:06:20   2/16/04  
!...Switches: -xf -plov -rflnrux2 
!**************************************************************
!                                                             *
! subroutines and functions called from this routine          *
!                                                             *
! no subroutines or functions called from this program unit   *
!                                                             *
!**************************************************************
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: l 
      double precision , intent(in) :: cp 
      double precision , intent(in) :: sp 
      double precision , intent(inout) :: q(-l:l) 
!-----------------------------------------------
!   L o c a l   P a r a m e t e r s
!-----------------------------------------------
      double precision, parameter :: anorm1 = .3989422804014326779399461D0  ! 1/sqrt(2*pi)
      double precision, parameter :: anorm = .5641895835477562869480795D0   ! 1/sqrt(pi)
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: m 
!-----------------------------------------------
!
      q(0) = anorm1 
      if (l == 0) return  
      q(1)      = cp*anorm 
      q((-1 ) ) = sp*anorm 
      do m = 2, l 
         q(m)      = q(m-1)*cp - q(1-m)*sp 
         q((-m ) ) = q(m-1)*sp + q(1-m)*cp 
      end do 
      return  
      end subroutine csphi1 
