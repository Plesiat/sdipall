#=================================================================================
#=================================================================================
# Compiler? Possible values: ifort; gfortran
F90 = gfortran
#
# Optimize? Empty: default No optimization; 0: No Optimization; -1 Debugging
OPT = -1
#=================================================================================


# Operating system, OS? automatic using uname:
OS=$(shell uname)
#=================================================================================
#=================================================================================
# ifort compillation
#=================================================================================
ifeq ($(F90),ifort)
   # opt management
   ifeq ($(OPT),0)
      F90FLAGS =
   else
      F90FLAGS = -O0 -g -check all -warn -traceback -debug extended -fpe0
   endif
endif
#=================================================================================
#=================================================================================



#=================================================================================
#=================================================================================
# gfortran (osx and linux)
#=================================================================================
 ifeq ($(F90),gfortran)
   # opt management
   ifeq ($(OPT),0)
      F90FLAGS =
   else
      ifeq ($(OPT),-1)
	F90FLAGS = -O0 -g -fbounds-check -fbacktrace -ffpe-trap=zero,overflow,underflow -O -fcheck=all
      else
	F90FLAGS = -O0 -g -Wall -Wextra -Warray-temporaries -Wconversion -fimplicit-none -fbacktrace -ffree-line-length-0 -fcheck=all -ffpe-trap=zero,overflow,underflow -finit-real=nan
      endif
   endif
endif



#=================================================================================
#=================================================================================
#$(info ***********************************************************************)
#$(info ***********OS:           $(OS))
#$(info ***********COMPILER:     $(F90))
#$(info ***********OPTIMIZATION: $(OPT))
#$(info ***********************************************************************)


F90_FLAGS = $(F90) $(F90FLAGS)
LYNK90 = $(F90_FLAGS)

SRCDIR = .
BINDIR = bin
LIBDIR1 = $(SRCDIR)/lib

PARLIBS= -fopenmp
SDIPLIB = objs
SDIPSRCS = $(sort $(wildcard $(SDIPLIB)/*.f90))
SDIPOBJS = $(SDIPSRCS:.f90=.o)

# ===================================================================================
# main

all: sdipall

sdipall: $(SDIPOBJS)
	$(LYNK90) $(SRCDIR)/sdipall.f90 -o $(BINDIR)/sdipall.exe $(SDIPOBJS) $(PARLIBS)


# ===================================================================================
# lib

$(SDIPLIB)/%.o: $(SDIPLIB)/%.f90
	$(F90_FLAGS) -c -o $@ $<

#
#
#===============================================
#===============================================
clean:
	find . -iname '*.o' -exec rm -f {} \;
	find . -iname '*.mod' -exec rm -f {} \;
	@echo "  done cleaning up"
very-clean:
	find . -iname '*.o' -exec rm -f {} \;
	find . -iname '*.mod' -exec rm -f {} \;
	rm -f $(BNDIR)/*.exe
	@echo "  done cleaning up"
#===============================================
#===============================================
#
