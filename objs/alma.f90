
  subroutine avgalm(ndegb,lmax,dlm,alm)
  
  implicit none
  
  integer, intent(in) :: ndegb, lmax
!   complex*16, intent(in) :: dlm(3,nsyma,0:lmax,-lmax:lmax)
  complex*16, intent(in) :: dlm(ndegb,3,0:lmax,-lmax:lmax)
  complex*16, intent(out) :: alm(ndegb,0:2*lmax,-2*lmax:2*lmax)
  
  integer k, mu, l, m, l1, l2, m1, m2
  double precision a3j, d
  complex*16 a, b, c
  
  double precision pig, fac
  parameter(pig=4.d0*datan(1.d0), fac = 1.d0/(3.d0*dsqrt(4.d0*pig)))
  
  alm = 0.d0
  
        do l1=0, lmax
          do l2=0, lmax
!             write(*,*) l1, l2, cc(l1)*dconjg(cc(l2))
            do L=iabs(l1-l2), l1+l2, 2
              
              a = fac*dsqrt((2.d0*l1+1.d0)*(2.d0*l2+1.d0)*(2.d0*L+1.d0))*a3j(l1,l2,L,0,0,0)
!               write(*,*) l1, l2, L, a3j(l1,l2,L,0,0,0), a*3.d0
              do m1=-l1, l1
                
                b = a*(-1.d0)**(m1)
                do m2=-l2, l2
                  M = m1-m2
                  c = b*a3j(l1,l2,L,-m1,m2,M)
                  
                  do mu=1, 3
                  do k=1, ndegb
                  alm(k,L,M) = alm(k,L,M)+c*dlm(k,mu,l1,m1)*dconjg(dlm(k,mu,l2,m2))
                  enddo
!                   if ( dabs(dimag(c*dlm(mu,l1,m1)*dconjg(dlm(mu,l2,m2)))) > 1.d-20 ) then
!                   write(*,*) l1, l2, L, m1, m2, mu, c*dlm(mu,l1,m1)*dconjg(dlm(mu,l2,m2))
!                   endif
!                     write(*,'(6i3,4e15.7)') l1, l2, L, m1, m2, mu, c*dlm(mu,l1,m1)*dconjg(dlm(mu,l2,m2)), alm(L,M)
                enddo !mu
                enddo !m2
              enddo !m1
            enddo !L
          enddo !l2
        enddo !l1
  
!                       do l=0, 2*lmax
!                         do m=-l, l
!                             write(*,*) l, m, alm(l,m)
!                         enddo
!                     enddo

  return
  
  end subroutine avgalm
  
  
  
  subroutine labal0(ndegb,lmax,dlm,al0)
  
  implicit none
  
  integer, intent(in) :: ndegb, lmax
  complex*16, intent(in) :: dlm(ndegb,3,0:lmax,-lmax:lmax)
  complex*16, intent(out) :: al0(ndegb+1,0:2)
  
  integer k, mu, mu1, mu2, L, l1, l2, li, le, M, m1, m2
  double precision a3j
  complex*16 a, b, c, d
  
  double precision pig, fac
  parameter(pig=4.d0*datan(1.d0), fac = 1.d0/(3.d0*dsqrt(4.d0*pig)))
  double precision sqrm3, sqr3m2, sqr10m3
  PARAMETER (SQRM3=0.577350269189d0, SQR3M2=1.22474487139d0, SQR10M3=1.82574185835d0)
  
  al0 = 0.d0
  
        do l1=0, lmax
          li = max (0, l1 - 2)
          le = min (lmax, l1+ 2)
          do l2=li, le
            a = dsqrt((2.d0*l1+1.d0)*(2.d0*l2+1.d0))
            do L=abs(l1-l2), min(2, l1 + l2), 2
              b = a*a3j(l1,l2,L,0,0,0)
              do m1=-l1, l1
                do m2=-l2, l2
                  M = m1-m2
                  c = b*a3j(l1,l2,L,-m1,m2,M)
                  do mu1=-1, 1
                    do mu2=-1, 1
                      mu = mu1-mu2
                      if (M /= mu) cycle
                      do k=1, ndegb
                      al0(k+1,L) = al0(k+1,L)+c*(-1.d0)**(m1+mu1)*a3j(1,1,L,mu2,-mu1,mu)* &
                               dlm(k,mu1+2,l1,m1)*dconjg(dlm(k,mu2+2,l2,m2))
                      enddo
                    enddo !mu2
                  enddo !mu1
                enddo !m2
              enddo !m1
            enddo !l
          enddo !l2
        enddo !l1
        
        do k=1, ndegb
        al0(1,:) = al0(1,:)+al0(k+1,:)
        enddo
!                 al0 = (-1)**mr * (2L+1) * a3j(1,1,L,mr, -mr, 0) *al0
        al0(:,0) = - SQRM3 * al0(:,0)
        al0(:,1) = -      SQR3M2 * al0(:,1)
        al0(:,2) =  SQR10M3 * al0(:,2)

  end subroutine labal0
