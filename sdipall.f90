!======================================================!
!* File: sdipall.f90
!* Creation: 09.2017 (Étienne Plésiat)
!* Last revision: 28.10.2020 (Étienne Plésiat)
!-------------------------------------------------------------------------------------------!
!* Function: sdipall takes dipole couplings as input and calculate cross
!   sections, MFPADs, phases and Wigner time delays as a function of the
!   photon and  photoelectron energy
!* Usage:
!   - sdipall requires to read an input file "sdipall.inp" containing
!     information about the dipole coupling filenames, their labels, IPs, etc.
!     in order to compute the cross sections.
!   - To compute additional magnitudes such as the MFPADs and Wigner time
!     delays, sdipall requires to read additional input files: "selrul.inp" and
!     "symang.inp".
!   - To include 1D nuclear motion in the Born-Oppenheimer approximation,
!     sdipall requires to read an additional input file "vibmod.inp" containing
!     the filenames of the vibrational energies and wavefunctions.
!======================================================!

    program sdipall

      use omp_lib

      implicit none

      integer i, j, k, l, m, n, s, p, q, r, d, c, u, v, w

      double precision, dimension(:, :, :), allocatable :: mat, fvib
      double precision, allocatable :: csnc(:, :), csns(:, :), csno(:, :, :, :), csnt(:, :, :), csna(:, :, :), cst(:, :)
      double precision, allocatable :: ioner(:)
      double precision, allocatable :: phold(:), egrid(:), edp(:), enr(:, :, :), enew(:), deren(:), evib(:, :)
      integer, dimension(:), allocatable :: wsym, ilig, icol, np, ies
      integer nre(2), nce(2), nrf(2), ncf(2), v1, v2, v2min, v2max, nrmax
      integer, dimension(:, :), allocatable :: nss
      character*50, dimension(:), allocatable :: dipname
      character*50 vibname(2)

      double precision, allocatable :: rgrid(:), ifvib(:, :), idvib(:, :, :, :), dip(:, :, :), eions(:)

      integer nvecb, nstate, nb, al, nemc, nenem
      integer iir, nr, io, nkm, npmax
      integer rank
        
      integer nee, nener0, nveca, ndega, ndegb
      integer, allocatable :: nener(:)
      integer, allocatable :: nmuka(:), nnmka(:, :), selrul(:, :), isym(:), idega(:), idegb(:), isyma(:, :), ntrans(:)
      character*50 s_tmp
      character*10, dimension(:), allocatable :: symb, labs
      double precision, allocatable :: csnp(:, :, :), csnl(:,:), csnlm(:,:, :)
      character*1 choice, dipc(3), emode
      character*2 cphi
      character*4 fstr(2)

      double precision rmin, rmax, rshift
      double precision emm(2), estp, ethd, eir, d_tmp(4)
      double precision, allocatable :: dmax(:), dmmax(:)
      character*50 outname
      character*10 num1, num2, num3, scomp

      integer nigl0, nogl, nigl1(2), nigl2(2), nigl3, prtpad
      double precision yang, pig, a0, sfine, eV, y11, const1, const2, const3, atto2au, solfac, eps, deg2rad
      complex*16 zi
      parameter(pig=4.d0*datan(1.d0), a0=5.2917720859D-11, sfine=7.2973525376D-03, eV=27.2113845d0, deg2rad=pig/180.d0)
      parameter(yang=pig, nigl0=30, nigl3=50, nogl=30)
      parameter(const1=(2.d0*4.d0*pig*pig*sfine/3.d0)*(a0*a0)*1.D28*1.D-06, const2=const1*3.d0, const3=const1*4.d0*pig)
      parameter(zi=(0.d0, 1.d0), y11=dsqrt(4.d0*pig/3.d0), atto2au=41.341373336D-03, solfac=1./(4.*pig))
      parameter(eps=1.d-15, fstr=(/"_ee-", "_ep-"/))
      logical testgeo, testvib, testfile, testrul, testsym, testnan, testang, testad, calapad, calalab, testdiv, ltmp
      logical prtiad, prtxtie,  intphi, rotang, dointe, doigl1(2), doigl2(2), parelf, phasor, prtddtp
      logical calwtie0, calttie0, calwtie, calttie

      integer im, it, ip, ia, ntang1, ntang2, nang1(2), nang2(2), nangx(2), idang1, nlmax, nbmax, mu, lmax, nmfmax
      integer ideir, ddeir
      double precision inang1, dcp(3), phno, vi(3), vf(3), xyz(3)
      double precision sigl, mfac, efac, xtia, ddenr
      integer, allocatable :: nla(:), npa(:, :), ndg(:), ilf(:, :), ihf(:, :), nmf(:, :, :), mmf(:, :, :)
      double precision, allocatable :: ang0(:, :), ang1(:, :), ang2(:, :), solang(:)
      double precision, allocatable :: bcf(:, :, :), angr1(:, :), angr2(:, :), xtna(:), ctna(:), phnp(:, :, :), cdnt(:), btnk(:, :)
      double precision, allocatable :: mfpad(:, :, :, :), mfkad(:, :, :, :, :), mtpad(:, :, :, :), mfmad(:, :, :, :)
      double precision, allocatable :: wtpad(:, :, :, :), ttpad(:, :, :, :), tmpad(:, :, :), dpad(:, :), opad(:)
      double precision, allocatable :: wtnf(:, :, :), ctnf(:, :, :), csnk(:, :), ctne(:), ctia(:)
      double precision, allocatable :: phol2(:, :), bbw(:, :, :, :), tdnf(:, :, :, :)
      complex*16, allocatable :: cc(:, :), spha(:, :), alm(:, :, :), b2w(:, :, :), dsr1(:, :, :, :), dsr2(:, :), dsrp(:, :, :)
      complex*16 ddiff, ddsum, c_tmp(5), wdm(3)
      double precision amin(2), amax(2), glxi(nogl), glws(nogl), glst3, glst1(2), glst2(2)
      double precision interpo4, a3j, glint, phasoradd
      
      integer nseq(2)
      double precision, allocatable :: gridpar(:,:,:)
      integer initime1, initime2, crate

!       ddenr = 1.d-5
!       ddenr = -1.d0
      rank = 0

!!!$  print *,"Running in parallel with ",lentrim(OMP_get_max_threads()),"threads"
    
      prtddtp = .False.
      calwtie0 = .True.
      calttie0 = .True.
      prtxtie = .True.
      prtiad = .True.
      testad = .True.
      calapad = .False.
      calalab = .False.

      cphi = "pa"
      doigl1 = .False.
      doigl2 = .False.
      parelf = .False.
      
      phasor = .False.
!       phasor = .True.

      eir = 0.05695419d0

444   format(F7.2,a,F7.2,$)
555   format(2E15.7, $)

777   format(500E15.7)
888   format(2E15.7)
999   format(E15.7, $)

      dipc = (/"y", "z", "x"/)

      INQUIRE (FILE="sdipall.inp", EXIST=testfile)
      if (testfile .neqv. .True.) then
        write (*, *) "Error! Input file not found!"
        stop
      endif
      open (17, file="sdipall.inp")
      read(17,'(a,$)') emode
      
      if ( emode == "e" .or. emode == "p" ) then
        read(17,*) nee
        dointe = .True.
        if ( nee == 0 ) then
          read(17,*) nee, emm(1), emm(2)
          allocate(egrid(nee))
          estp = (emm(2)-emm(1))/dble(nee-1)
          do i=1, nee
            egrid(i) = emm(1)+(i-1)*estp
          enddo
        else
          allocate(egrid(nee))
          read(17,*) (egrid(i), i=1, nee)
        endif
        write(*,*) "* The dipole will be interpolated over the energy grid:"
        write(*,*) "* nee, emin, emax = ", nee, egrid(1), egrid(nee)
      else
        dointe = .False.
      endif
      
      read (17, *) nvecb, nveca, ethd
      
      v2min = 0
      v2max = 0
      INQUIRE (FILE="vibmod.inp", EXIST=testvib)
      if (testvib) then
        write (*, *) "* vibmod selected!"
        open (37, file="vibmod.inp")
        
        if ( dointe ) then
          write(*,*) "* Error! Energy interpolation not yet implemented with vibmod!"
          stop
        endif
      endif
      
      INQUIRE (FILE="geomod.inp", EXIST=testgeo)
      if (testgeo) then
        if (testvib) then
          write(*,*) "Error! vibmod.inp and geomod.inp cannnot be defined at the same time!"
          stop
        endif
        write (*, *) "* geomod selected!"
        open (47, file="geomod.inp")
        read(47,*) v2min, v2max
        close(47)
      endif

      INQUIRE (FILE="selrul.inp", EXIST=testrul)
      if (testrul) then
        write (*, *) "* Selection rule information found!"
      endif

      open (78, file="info.out")
      INQUIRE (FILE="symang.inp", EXIST=testang)
      if (testang) then
        write (*, *) "* Angular information found!"
        if (.not. testrul) then
          write (*, *) "Error! selrul.inp file is required when using symang.inp!"
          stop
        endif
        open (57, file="symang.inp")
        read (57, *) nlmax, nbmax
        allocate (nla(nveca), ndg(nveca), npa(nveca, 3))
        allocate (ilf(nveca, nlmax), ihf(nveca, nlmax), nmf(nveca, 3, nlmax), bcf(nveca, 3, nbmax), mmf(nveca, 3, nbmax))
        lmax = 0
        nmfmax = 0
        do i = 1, nveca
          read (57, *)
          read (57, *) s_tmp, s_tmp, nla(i), s_tmp, s_tmp, ndg(i)
          read (57, *)
          read (57, *) (ilf(i, j), ihf(i, j), j=1, nla(i))
          if (maxval(ilf(i, 1:nla(i))) > lmax) lmax = maxval(ilf(i, 1:nla(i)))
          do k = 1, ndg(i)
            read (57, *)
            read (57, *) (nmf(i, k, j), j=1, nla(i))
            read (57, *)
            npa(i, k) = sum(nmf(i, k, 1:nla(i)))
            read (57, *) (bcf(i, k, j), j=1, npa(i, k))
            read (57, *)
            read (57, *) (mmf(i, k, j), j=1, npa(i, k))

            n = maxval(nmf(i, k, :))
            if (n > nmfmax) nmfmax = n
          enddo
          read (57, *)
        enddo

        write (*, *) "* lmax =", lmax
        allocate (dmax(0:lmax), dmmax(0:lmax))
        dmmax = 0.d0

        read (57, *, iostat=io) ntang1
        write(*,*) "* ntang1 = ", ntang1

        prtpad = 0
        if (io /= 0) then
          testad = .False.
        else
          if (ntang1 == -1) then
            calapad = .True.
          elseif (ntang1 == -2) then
            calalab = .True.
          else
            write (*, *) "* wtie will be printed!"
            if (ntang1 == 0) then
              read (57,*) idang1, inang1
              inang1 = inang1*pig/180.d0
              prtxtie = .False.
              write (*, *) "* wtia will be printed!"
              write (*, *) "* Molecular angles are generated automatically..."
              
              do i = 1, 2
                read (57, *) nang1(i), amin(i), amax(i)
                amin(i) = amin(i)*pig/180.d0
                amax(i) = amax(i)*pig/180.d0
              enddo
              call angpar("molecular", amin, amax, nigl0, nogl, yang, pig, doigl1, nigl1, nang1, ntang1)

              allocate (ang0(2, maxval(nang1)), ang1(2, ntang1))
              do i = 1, 2
                if (doigl1(i)) then
                  call gaussweight(glxi, glws, nogl)
                  call glpar(amin(i), amax(i), glxi, nigl1(i), nogl, nang1, i, ang1(i, :), glst1(i))
                else
                  call angrid((/nang1(i), 1/), 1, amin(i), amax(i), ang0(i, 1:nang1(i)))
                  call angrid(nang1, i, amin(i), amax(i), ang1(i, :))
                endif
              enddo
              
              allocate(solang(nang1(1)))
              do i=1, nang1(1)
                ia = (i - 1)*nang1(2) + 1
                solang(i) = dsin(inang1+dabs(ang1(1, ia)-ang1(1, idang1)))
              enddo
              write (*,*) "* Solid angles: ", (solang(i), i=1, nang1(1))
              
              mfac = dabs(-dcos(amax(1)) + dcos(amin(1)))
              d_tmp(1) = amax(2)-amin(2)
              if ( d_tmp(1) /= 0.d0) mfac = mfac*d_tmp(1)
              mfac = 1.d0/mfac
              write (*, *) "* mfac = ", mfac
              if ( mfac == 0.d0 .or. mfac /= mfac .or. mfac > HUGE(mfac) ) write (*, *) "* Setting mfac to 1..."

            else

              allocate (ang1(2, ntang1))
              do i = 1, ntang1
                read (57, *) ang1(1, i), ang1(2, i)   ! beta, alpha
              enddo
              ang1(:, :) = ang1(:, :)*pig/180.d0
              doigl1 = .False.
            endif
          endif

          read (57, *) nseq
          
          allocate(gridpar(2,maxval(nseq),3))
          do i=1, 2
            read(57,*) (gridpar(i,j,:), j=1, nseq(i))
            gridpar(i,1:nseq(i),2:3) = gridpar(i,1:nseq(i),2:3)*pig/180.d0
            nang2(i) = int(sum(gridpar(i,1:nseq(i),1)))
            amin(i) = minval(gridpar(i,1:nseq(i),2))
            amax(i) = maxval(gridpar(i,1:nseq(i),3))
          enddo
          
          call angpar("electron", amin, amax, nigl0, nogl, yang, pig, doigl2, nigl2, nang2, ntang2)
          if (nang2(2) == 1) cphi = "po"

          ! prtpad = 0 (no AD), 1 (csie) 2 (only MFPAD), 3 (+wtpad), 4 (+ttpad, +mtpad)
          read (57, *) prtpad, intphi, rotang
          
          nangx = nang2
          if ( intphi ) nangx(2) = 1

          if (prtpad > 1) then
            write (*, *) "* MFPADS will be written..."
            if (nvecb*ntang1 > 30) then
              write(*,*) "Warning! Number of individual MFPADs is large: ", nvecb*ntang1
!               write(*,'(a,$)') " > Do you want to continue? (y/n) "
!               read (*,*) choice
              choice = "y"
              if ( choice /= "y" .and. choice /= "Y" ) then
                prtpad = 2
                prtiad = .False.
                write (*, *) "* Individual MFPADs will be skipped..."
              endif
            endif
            if (ntang2 == 1) then
              prtpad = 0
              write (*, *) "* Individual MFPADs, calwtie and calttie will be skipped as ntang2 == 1..."
            endif
          endif
          if (prtpad == 0) then
            calwtie0 = .False.
            calttie0 = .False.
          endif
          if (intphi) then
            write (*, *) "* Phi angles will be integrated..."
            cphi = "pi"
          endif
          if (rotang) then
            rotang = .True.
            write (*, *) "* Electron angles will be rotated..."
            read (57, *) vi
            read (57, *) vf
          endif
          
          read(57, *, iostat=io) prtddtp
          if ( io /= 0 ) prtddtp = .False.
          if ( prtddtp ) write (*, *) "* Fully differential complex dipoles will be written in unformatted file..."

          if (amax(1) - amin(1) /= pig .or. amax(2) - amin(2) /= 2.d0*pig) then
            write (*, *) "Warning! Electron angles do not span the entire angle interval!"
          endif

          efac = 1.d0
          do i = 1, 2
            if (amax(i) == 0.d0) then
              efac = efac*dble(i)*pig
            else
              efac = efac*dble(i)*pig/(amax(i))
            endif
          enddo
          write (*, *) "* efac = ", efac

          if (nang2(1) == 0 .or. nang2(2) == 0) then
            prtpad = 0
            calwtie0 = .False.
            write (*, *) "Warning! No electron angles: calculation of wtie and pad deactivated!"
          endif

          allocate (ang2(2, maxval(nang2)))
          do i = 1, 2
            if (doigl2(i)) then
              call gaussweight(glxi, glws, nogl)
              call glpar(amin(i), amax(i), glxi, nigl2(i), nogl, (/nang2(i), 1/), 1, ang2(i, 1:nang2(i)), glst2(i))
            else
              call getgrid(nseq(i),gridpar(i,1:nseq(i),:),ang2(i, 1:nang2(i)))
!               call angrid((/nang2(i), 1/), 1, amin(i), amax(i), ang2(i, 1:nang2(i)))
            endif
          enddo
          deallocate(gridpar)

          allocate (angr1(2,ntang1), angr2(2, ntang2))
          if (rotang) then
            do im=1, ntang1
              xyz = (/1.d0, ang1(1, im), ang1(2, im)/)
              call cart2sph(xyz, 1, "radian", -1)
              call rotmatavec(xyz, 1, vi, vf)
              call cart2sph(xyz, 1, "radian", 1)
              angr1(:, im) = xyz(2:3)
!               if ( angr1(1, im) > pig/2.d0 ) angr1(1, im) = pig-angr1(1, im)
!               if ( angr1(2, im) > pig ) angr1(2, im) = 2.d0*pig-angr1(2, im)
            enddo
            
            ia = 1
            do it = 1, nang2(1)
              do ip = 1, nang2(2)
                xyz = (/1.d0, ang2(1, it), ang2(2, ip)/)
                call cart2sph(xyz, 1, "radian", -1)
                call rotmatavec(xyz, 1, vf, vi)
                call cart2sph(xyz, 1, "radian", 1)
                angr2(:, ia) = xyz(2:3)
                ia = ia + 1
              enddo
            enddo
          else
            
            do im=1, ntang1
              angr1(:, im) = (/ang1(1, im), ang1(2, im)/)
            enddo
          
            ia = 1
            do it = 1, nang2(1)
              do ip = 1, nang2(2)
                angr2(:, ia) = (/ang2(1, it), ang2(2, ip)/)
                ia = ia + 1
              enddo
            enddo
          endif

          ! If we set only one electronic angle with values 180 360, we study electronic emission parallel to laser field
          if (ntang2 == 1) then
            if (dabs(ang2(1, 1) - pig) <= eps .and. dabs(ang2(2, 1) - 2.d0*pig) <= eps) then
              write (*, *) "Parallel emission mode selected..."
              parelf = .True.
            endif
          endif

        endif ! ntang1=-1

        close (57)

        write (78, *) "# Integration parameters (molecule): ", nigl1
        write (78, *) "# Integration parameters (electron): ", nigl2
        write (78, *)

        write (78, *) "# Angular grid (molecule):"
        do i = 1, ntang1
          write (78, *) ang1(1, i), ang1(2, i)
        enddo
        write (78, *)
        write (78, *) "# Angular grid (electron):"
        do it = 1, nang2(1)
          do ip = 1, nang2(2)
            write (78, *) ang2(1, it), ang2(2, ip)
          enddo
        enddo
        if (rotang) then
          write (78, *)
          write (78, *) "# Angular grid (rotated): "
          do i = 1, ntang2
            write (78, *) angr2(1, i), angr2(2, i)
          enddo
        endif
        write (78, *)

      endif
      
      
       if ( prtddtp ) then
              open (222, file="ddtp.out", form='unformatted')
              write (222) nvecb
              write (222) ntang1, nang2(1), nang2(2)
              write (222) (ang1(1, im), im=1, ntang1)
              write (222) (ang1(2, im), im=1, ntang1)
              write (222) (angr1(1, im), im=1, ntang1)
              write (222) (angr1(2, im), im=1, ntang1)
              write (222) (ang2(1, it), it=1, nang2(1))
              write (222) (ang2(2, ip), ip=1, nang2(2))
          endif
      
      

      allocate (nener(nvecb), labs(nvecb))

      do c = 1, nvecb

        read (17, *) labs(c), nstate, ndegb, nr
        if (nr /= 0) then
          allocate (eions(nr))
          read (17, *) (eions(i), i=1, nr)
        endif
        write (*, *)
        write (*, '(a,a,i3,a,i3,a)') "=========== Initial symmetry ", trim(labs(c)), c, "/", nvecb, " ==========="

        allocate (wsym(nstate))
        read (17, *) (wsym(i), i=1, nstate)
        allocate (ilig(nstate))
        allocate (icol(nstate))
        allocate (dipname(nstate))
        do i = 1, nstate
          read (17, *) dipname(i)
        enddo

        do i = 1, nstate
          call colig(dipname(i), ilig(i), icol(i), "show")
        enddo

        do i = 2, nstate
          if (ilig(i) /= ilig(1)) then
            write (*, *) "Error!!! Inconsitent number of lines: ilig(1), ilig(i), i:", ilig(1), ilig(i), i
            stop
          endif
        enddo

        allocate (mat(nstate, ilig(1), maxval(icol)))

        do i = 1, nstate
!         call lect3(mat,i,ilig(i),icol(i),dipname(i))
          open (8, file=trim(dipname(i)))
          do j = 1, ilig(i)
            read (8, *) (mat(i, j, k), k=1, icol(i))
          enddo
          close (8)
        enddo

        do i = 1, nstate

          k = 2
          do while (mat(i, k, 1) == mat(i, k - 1, 1))
            k = k + 1
            if (k > ilig(1)) exit
          enddo
          k = k - 1

          if (i == 1) then
            nener0 = k
            al = ilig(i)/nener0
          else
            if (k /= nener0) then
              write (*, *) "Error!!! Inconsitent number of energies: nener0(1), nener0(i), i:", nener0, k, i
              stop
            endif
          endif

        enddo

        allocate (np(nstate))
        do i = 1, nstate
          np(i) = (icol(i) - 2)/2
        enddo

        write (*, *)
        write (*, *) "----- Electronic part -----"
        write (*, *) "* Number of states:", nstate
        write (*, *) "* Number of coordinates:", al
        write (*, *) "* Number of energies:", nener0
        write (*, *) "* Number of partial waves:", (np(i), i=1, nstate)
        write (*, *)
        
        ! When we interpolate in energy
        if ( dointe ) then
          nener(c) = nee
        else
          nener(c) = nener0
        endif
        
        if (c == 1) then
          allocate (cst(nvecb, nener(c)), enr(2, nvecb, nener(c)), deren(nener(c)), ioner(nvecb))
          if (testrul) allocate (csnt(nvecb, nener(c), 3))
          if (prtpad > 1) allocate (mfkad(nvecb, ntang1, nener(c), nangx(1), nangx(2)))
        else
          if (nener(c) > nener(1)) then
            write (*, *) "Error! The number of energy points in the grid must be lower than initial one!"
            stop
          endif
        endif
        
        calwtie = calwtie0
        calttie = calttie0
        if ( nener(c) < 3 ) then
          write(*,*) "Warning! wtie calculation require at least three points in energy!"
          write(*,*) "wtie calculation is disabled..."
          calwtie = .False.
        endif

        if (testvib) then
          rewind(37)
          do while (.True.)
            read (37, *) s_tmp
            if (trim(s_tmp) == trim(labs(c))) then
              read (37, *) v1
              read (37, *) v2min, v2max
              do i = 1, 2
                read (37, *) vibname(i)
                call colig(vibname(i), nre(i), nce(i), "show")
              enddo
              allocate (evib(2, maxval(nre*nce)))
              do i = 1, 2
                open (8, file=trim(vibname(i)))
                read (8, *) (evib(i, j), j=1, nre(i)*nce(i))
                close (8)
              enddo
              do i = 1, 2
                read (37, *) vibname(i)
                call colig(vibname(i), nrf(i), ncf(i), "show")
              enddo
              nrmax = maxval(nrf)
              allocate (fvib(2, nrmax, maxval(ncf)))
              do i = 1, 2
                open (8, file=trim(vibname(i)))
                do j = 1, nrf(i)
                  read (8, *) (fvib(i, j, k), k=1, ncf(i))
                enddo
                close (8)
              enddo

              rmin = max(fvib(1, 1, 1), fvib(2, 1, 1))
              rmax = min(fvib(1, nrf(1), 1), fvib(2, nrf(2), 1))
              write (*, *)
              write (*, *) "----- Nuclear part -----"
              write (*, *) "* v1max:", ncf(1) - 2
              write (*, *) "* v2max:", ncf(1) - 2
              write (*, *) "* nrs:", nrf(1), nrf(2)
              write (*, *) "* rmin:", rmin
              write (*, *) "* rmax:", rmax

              if (v1 > ncf(1) - 2) then
                write (*, *) "Error! v1 is larger than vmax!"
                stop
              endif
              if (v2 > ncf(2) - 2) then
                write (*, *) "Error! v2 is larger than vmax!"
                stop
              endif
              exit
            endif
          enddo
        endif

        if (testrul) then
          allocate (idegb(nstate))
          allocate (nmuka(nveca))
          allocate (nnmka(ndegb, 3))
          allocate (selrul(nstate, 3))
          allocate (symb(ndegb))
          allocate (isym(nstate), idega(nstate))
          allocate (isyma(ndegb, nstate))
          nnmka = 0
          n = 0
          i = 1
          open (27, file="selrul.inp")
          do while (i <= ndegb)
            s_tmp = " "
            do while (s_tmp(1:1) /= "@")
              read (27, '(a)') s_tmp
            enddo
            symb(i) = trim(s_tmp(SCAN(trim(s_tmp), " ") + 1:))
            read (27, *) s, s, d
            read (27, *) (nmuka(j), j=1, nveca)
            read (27, *)

            if (trim(symb(i) (1:len(trim(labs(c))))) == trim(labs(c)) .and. i == d) then
              m = 0
              do j = 1, nveca
                if (n + nmuka(j) > nstate) then
                  write (*, *) "Error! n+nmuka(j) > nstate"
                  stop
                endif
                if (nmuka(j) /= 0) then
                  read (27, *) ((selrul(n + k, l), l=1, 3), k=1, nmuka(j))

                  do k = 1, nmuka(j)
                    if (n >= nstate) then
                      write (*, *) "Error! Trying to read more states than expected: n, nstate=", n, nstate
                      stop
                    endif
                    m = m + 1
                    isyma(i, m) = j
                    isym(n + k) = j
                    l = selrul(n + k, 2)
                    nnmka(i, l) = nnmka(i, l) + 1
                    idega(n + k) = selrul(n + k, 1)
                    idegb(n + k) = d
                  enddo
                  n = n + nmuka(j)
                endif
              enddo
              i = i + 1
            else
              do j = 1, nveca
                if (nmuka(j) /= 0) read (27, *)
              enddo
            endif
          enddo

          close (27)
          if (n /= nstate) then
            write (*, *) "Error! Inconsistent number of states: n, nstate=", n, nstate
            stop
          endif
          deallocate (nmuka)

          ndega = maxval(idega)

          allocate (ntrans(ndegb))
          do k = 1, ndegb
            ntrans(k) = sum(nnmka(k, :))
          enddo !k
          write (*, *) "* ntrans = ", ntrans

        endif ! testrul

        if (testang) then
          do i = 1, nstate
            if (np(i) /= nla(isym(i))) then
              write (*, *) "Error! Inconsistent number of l: i, isym(i), np(i), nla(i) = ", i, isym(i), np(i), nla(isym(i))
              stop
            endif
          enddo
        endif

        allocate(edp(nener0))
        allocate (dip(nstate, nener(c), maxval(icol) - 2))

        do i = 1, nstate
          do k = 1, al
            do j = 1, nener0
              if (i == 1 .and. k == 1) then
                edp(j) = mat(i, (k - 1)*nener0 + j, 2)
              else
                if (mat(i, (k - 1)*nener0 + j, 2) /= edp(j)) then
                  write (*, *) "Error! Inconsistent energy grid!"
                  stop
                endif
              endif
            enddo
          enddo
        enddo
        
        if (testvib) then

          if (al < 10) then
            write (*, *) "Error! Not enough coordinates in coupling file to include nuclear motion!"
            stop
          endif

          nrmax = nigl3*nogl
          d_tmp(1) = nstate*nener(c)*maxval(icol)*nrmax*8.d0*1.d-09
          write (*, '(a,f8.3,a)') "* Interpolating dipole couplings (required memory ", d_tmp(1), " GB)..."
          if (d_tmp(1) > 8) then
            write (*, *) "Error! Required memory is too large!"
            stop
          endif

!       allocate(rgrid(nrmax), ifvib(2,nrmax), idvib(nstate,nener(c),maxval(icol),nrmax))
!       do k=1, nrmax
!         rgrid(k) = mat(1,1,1)+(k-1)*(mat(1,(al-1)*nener(c)+1,1)-mat(1,1,1))/(nrmax-1)!+rshift
!       enddo
!       write(*,*) "* rmin, rmax = ", rgrid(1), rgrid(nrmax)
!       if ( rgrid(1) < rmin .or. rgrid(nrmax) > rmax ) then
!         write(*,*) "Error! Couplings grid interval is larger than vibrational WF grid interval!"
!         stop
!       endif
          allocate (rgrid(nrmax), ifvib(2, nrmax), idvib(nstate, nener(c), maxval(icol), nrmax))
          call gaussweight(glxi, glws, nogl)
          call glpar(mat(1, 1, 1), mat(1, (al - 1)*nener(c) + 1, 1), glxi, nigl3, nogl, (/nrmax, 1/), 1, rgrid, glst3)

          call interpogrid2(fvib(1, :, 1), fvib(1, :, v1 + 2), nrf(1), rgrid, ifvib(1, :), nrmax)
          do i = 1, nstate
            do j = 1, nener(c)
              do l = 3, icol(i)
                call interpogrid2(mat(i, j:ilig(i):nener(c), 1), mat(i, j:ilig(i):nener(c), l), &
                                  al, rgrid, idvib(i, j, l - 2, :), nrmax)
              enddo
            enddo
          enddo

        endif
        
        if ( .not. dointe ) enr(1, c, :) = edp ! Change later if dointe
        
        iir = 0
        do v2 = v2min, v2max

          if (testvib) then

            call nbtocar_z(v1, num1, 1)
            scomp = "_v"//trim(num1)//"-"
            call nbtocar_z(v2, num1, 3)
            scomp = trim(scomp)//trim(num1)
            ioner(c) = evib(2, v2 + 1) - evib(1, v1 + 1)
!         if ( rshift /= 0.d0 ) scomp=trim(scomp)//"_s_"

            call interpogrid2(fvib(2, :, 1), fvib(2, :, v2 + 2), nrf(2), rgrid, ifvib(2, :), nrmax)
            d_tmp(1) = glint(glws, glst3, ifvib(1, :)*ifvib(2, :), nigl3, nogl)
!         call intGL(rgrid,ifvib(1,:)*ifvib(2,:),nrmax,rgrid(1),rgrid(nrmax),d_tmp(1),nigl0,nogl)
            write (*, '(/,a,i4,a,e13.5)') "  ==> v2 = ", v2, " - FC: ", d_tmp(1)**2

            do i = 1, nstate
              do j = 1, nener(c)
!             dip(i,j,1) = mat(i,j,1)
!             print '(A,$)',char(27)//"[1A"
!             print '(A,i6,i6)', "istate, inener(c) = ", i, j
                do l = 1, icol(i) - 2
!               call interpogrid2(mat(i,j:ilig(i):nener(c),1),mat(i,j:ilig(i):nener(c),l),al,rgrid,ifvib(3,:),nrmax)
                  dip(i, j, l) = glint(glws, glst3, ifvib(1, :)*ifvib(2, :)*idvib(i, j, l, :), nigl3, nogl)
!               call intGL(rgrid,ifvib(1,:)*ifvib(2,:)*idvib(i,j,l,:),nrmax,rgrid(1),rgrid(nrmax),dip(i,j,l),nigl0,nogl)
                enddo !l
              enddo !j
            enddo !i

          else
            
            if ( testgeo ) then
              
              if ( v2min < 1 .or. v2max > al ) then
                write(*,*) "Error! Index of geometry out of range!"
                stop
              endif
              iir = v2
              
            else
              
                write (*, *) "* List of geometries:"
                do i = 1, al
                write (*, '(a,i2,a,f8.3)') "   R", i, " = ", mat(1, (i - 1)*nener(c) + 1, 1)
                enddo

                if (al == 1) iir = 1
                do while (iir < 1 .or. iir > al)
                iir = 1
                write (*, *)
                write (*, '(a,$)') " > Pick a geometry index: "
                read (*, *) iir
                enddo
            endif

            call nbtocar_z(iir, num1, 3)
            scomp = "_r"//trim(num1)
            
            if (iir <= nr) then
              ioner(c) = eions(iir)
              write (*, *) "* IP (eV):", ioner(c)
            else
              do while (ioner(c) < 0.d0)
                write (*, *)
                write (*, '(a,$)') " > IP(eV) = "
                read (*, *) ioner(c)
              enddo
            endif
            ioner(c) = ioner(c)/ev
            
            if ( dointe ) then
              if ( emode == "e" ) then
                enr(1, c, :) = egrid
              else
                enr(1, c, :) = egrid(:)-ioner(c)
              endif
              if ( ANY( enr(1, c, :) <= 0. ) ) then
                write(*,*) "Error! Negative values in the energy grid!"
                write(*,*) "* enr = ", enr(1, c, :)
                stop
              endif
            endif
            
            do i = 1, nstate
              do l = 3, icol(i)
                m = (iir - 1)*nener0+1
                n = m+nener0-1
                if ( dointe ) then
                  call interpogrid2(edp, mat(i,m:n, l), nener0, enr(1, c, :), dip(i, :, l - 2), nener(c))
                else
                  dip(i, :, l - 2) = mat(i, m:n, l)
                endif
              enddo
            enddo

          endif ! testvib
          
          enr(2, c, :) = enr(1, c, :) + ioner(c)
          
          deren(1) = enr(1, c, 2)-enr(1, c, 1)
          deren(nener(c)) = enr(1, c, nener(c))-enr(1, c, nener(c)-1)
          ltmp = .False.
          do j = 2, nener(c)-1
            deren(j) = enr(1, c, j+1)-enr(1, c, j-1)
            if ( dabs(deren(j)-deren(2)) > 1e-6 ) ltmp = .True.
          enddo
          
          if ( ltmp ) then
            write(*,*) "Warning! The energy grid is not uniform!"
            write(*,*)
          endif
          
          
          write (78, *) "# Energy grid: ", c
          do j = 1, nener(c)
            write (78, *) j, enr(1, c, j)*ev, enr(2, c, j)*ev
          enddo
          write (78, *)

          if (ethd >= 0.d0) then
            write (*, *) "* Smoothing point mode selected!"
            p = 1 ! Number of controling points
            do j = 1, nener(c)
              if (enr(1, c, j) >= ethd) then
                k = j
                exit
              endif
            enddo
            if (k <= p) k = p + 1

            n = 0
            m = 0
            do i = 1, nstate
              do l = 1, icol(i) - 2, 2
                do j = k, nener(c) - p
                  n = n + 1
                  testdiv = .False.
                  do q = 1, p
                    do r = 1, 3
                      s = j + (r - 2)*p
                      d_tmp(r) = dip(i, s, l)**2 + dip(i, s, l + 1)**2
                    enddo
                    d_tmp(4) = dabs(d_tmp(3) - d_tmp(1))
                    if (d_tmp(2) > eps) then
                      if (dabs(d_tmp(2) - d_tmp(1))/d_tmp(4) > 1.d0 .and. dabs(d_tmp(3) - d_tmp(2))/d_tmp(4) > 1.d0) then
                        testdiv = .True.
                        d = q
                      endif
                    endif
                  enddo !q
                  if (testdiv) then
                    m = m + 1
                    do r = 0, 1
                      d_tmp(1) = interpo4(enr(1, c, :), dip(i, :, l + r), nener(c), enr(1, c, j))
                      dip(i, j, l + r) = d_tmp(1)
                    enddo
                  endif
                enddo
              enddo
            enddo
            write (*, '(a,f5.3,a)') " * Perc. of smoothed points: ", 100.d0*m/n, "%"
            write (*, *)
          endif

          allocate (csns(nstate, nener(c)))
          npmax = maxval(np)
          allocate (csnp(nstate, nener(c), npmax))
          allocate (phold(npmax))

          do w = 1, 2
            csns = 0.d0
            do i = 1, nstate
              phold = 0.d0
              call nbtocar_z(i, num2, 2)
              outname = trim(scomp)//"_s"//trim(num2)
              if (testrul) outname = trim(outname)//"_"//trim(dipc(selrul(i, 2)))
              outname = trim(outname)//".out"

              call ofile(2, 49, (/"phnp", "mdnp"/), labs(c), fstr(w), outname, "-", ioner(c)*ev)

              do j = 1, nener(c)
                testnan = .False.
                do k = 1, icol(i) - 2
                  if (dip(i, j, k) /= dip(i, j, k)) then
                    testnan = .True.
                    csns(i, j) = dip(i, j, k)
                  endif
                enddo

                if (testnan) then
                  write (*, *) "Warning! NaN detected for state ", i, " column ", j, ". Skipping it..."
                else
                  write (49, 999) enr(w, c, j)*ev
                  write (50, 999) enr(w, c, j)*ev
                  l = 0
                  do k = 1, icol(i) - 3, 2
                    l = l + 1
                    csnp(i, j, l) = dip(i, j, k)**2 + dip(i, j, k + 1)**2
                    write (50, 999) dsqrt(csnp(i, j, l))
                    csnp(i, j, l) = enr(2, c, j)*const2*csnp(i, j, l)
                    csns(i, j) = csns(i, j) + csnp(i, j, l)
                    call rebatan(phold(l), d_tmp(1), dip(i, j, k), dip(i, j, k + 1), 2)
                    write (49, 999) d_tmp(1)
                  enddo
                  call cwfile(2, 49, 1)
                endif
              enddo
              call cwfile(2, 49, 0)
            enddo
          enddo

          deallocate (phold)

          do w = 1, 2
            call ofile(2, 46, (/"csnp", "csns"/), labs(c), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
            write(47,*) "# Symmetries=", (i, i = 1, nstate)
            do j = 1, nener(c)
              cst(c, j) = 0.d0
              write (46, 999) enr(w, c, j)*ev
              write (47, 999) enr(w, c, j)*ev
              do i = 1, nstate
                cst(c, j) = cst(c, j) + dble(wsym(i))*csns(i, j)/3.d0    ! divided by 3 because it is the average
              enddo
              write (46, 999) cst(c, j)
              write (47, 999) cst(c, j)
              do i = 1, nstate
                do l = 1, np(i)
                  write (46, 999) csnp(i, j, l)
                enddo
                write (47, 999) csns(i, j)
              enddo
              call cwfile(2, 46, 1)

            enddo
            call cwfile(2, 46, 0)
          enddo

          do j = 1, nener(c)
            do i = 1, nstate
              do l = 1, np(i)
                csnp(i, j, l) = csnp(i, j, l)/3.d0
              enddo
            enddo
          enddo

          if (testrul) then
            allocate (csno(nener(c), ndegb, 3, nstate + 1), csnk(nener(c), ndegb))
            allocate (nss(ndegb, 3))

            csnk = 0.d0
            csno = 0.d0
            csnt(c, :, :) = 0.d0
            n = 0
            nss = 1
            do k = 1, ndegb
              do l = 1, ntrans(k)
                n = n + 1
                nss(k, selrul(n, 2)) = nss(k, selrul(n, 2)) + 1
                i = nss(k, selrul(n, 2))
!                 write(*,*) k, l, n, selrul(n, 2), csns(n, 1)
                do j = 1, nener(c)
                  csno(j, k, selrul(n, 2), i) = csno(j, k, selrul(n, 2), i) + csns(n, j)
                  csno(j, k, selrul(n, 2), 1) = csno(j, k, selrul(n, 2), 1) + csns(n, j)
                  csnt(c, j, selrul(n, 2)) = csnt(c, j, selrul(n, 2)) + csns(n, j)
                  csnk(j, k) = csnk(j, k) + csns(n, j)
                enddo
              enddo
            enddo
            csnk = csnk/3.d0

            do w = 1, 2
              call ofile(1, 50, (/"csno"/), labs(c), fstr(w), trim(scomp)//".out", "Orientations= Average x y z", ioner(c)*ev)
              do j = 1, nener(c)
                write (50, 777) enr(w, c, j)*ev, sum(csnt(c, j, :))/3.d0, &
                csnt(c, j, 3), csnt(c, j, 1), csnt(c, j, 2) !(csnt(c, j, l), l=1, 3)
              enddo
              call cwfile(1, 50, 0)
              do k = 1, ndegb
                call ofile(1, 51, (/"csno"/), symb(k), fstr(w), trim(scomp)//".out", "Orientations= Average x y z", ioner(c)*ev)
                do j = 1, nener(c)
                  write (51, 777) enr(w, c, j)*ev, csnk(j, k)/3.d0, &
                  sum(csno(j, k, 3, :)), sum(csno(j, k, 1, :)), sum(csno(j, k, 2, :)) !(sum(csno(j, k, l, :)), l=1, 3)
                enddo
                call cwfile(1, 51, 0)
                do l = 1, 3
                  call ofile(1, 52, (/"csns"/), symb(k), fstr(w), trim(scomp)//"_"//dipc(l)//".out", "-", ioner(c)*ev)
                  write (52, *) "# Symmetries=", (i, i=1, nss(k, l))
                  do j = 1, nener(c)
                    write (52, 777) enr(w, c, j)*ev, (csno(j, k, l, i), i=1, nss(k, l))
                  enddo
                  call cwfile(1, 52, 0)
                enddo
              enddo
            enddo

            deallocate (nss)
            deallocate (csno)

          endif ! testrul

          if (testang) then

            do w = 1, 2
              call ofile(1, 46, (/"csnh"/), labs(c), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
              call ofile(1, 50, (/"csnl"/), labs(c), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
              do mu=1, 3
                call ofile(1, 46+mu, (/"csnh"/), labs(c), fstr(w), trim(scomp)//"_"//dipc(mu)//".out", "-", ioner(c)*ev)
                call ofile(1, 50+mu, (/"csnl"/), labs(c), fstr(w), trim(scomp)//"_"//dipc(mu)//".out", "-", ioner(c)*ev)
              enddo
              allocate (csnlm(3,0:lmax, -lmax:lmax))
              allocate (csnl(3,0:lmax))
              dmax = 0.d0
              do j = 1, nener(c)
                d_tmp(1) = 0.d0
                csnlm = 0.d0
                csnl = 0.d0
                do i = 1, nstate
                  k = 0
                  mu = selrul(i, 2)
                  do p = 1, nla(isym(i))
                    do q = 1, nmf(isym(i), idega(i), p)
                      k = k + 1
                      if (k <= np(i)) then
                        l = ilf(isym(i), p)
                        m = mmf(isym(i), idega(i), k)
                        csnlm(mu,l, m) = csnlm(mu,l, m) + csnp(i, j, k)
                        csnl(mu,l) = csnl(mu,l) + csnp(i, j, k)
                        d_tmp(1) = d_tmp(1) + csnp(i, j, k)
                      endif
                    enddo
                  enddo
                enddo
                if (dabs(cst(c, j) - d_tmp(1)) > 1.d-10) then
                  write (*, *) "Warning! Inconsistent total cross section! ee, cst, d_tmp = ", enr(1, c, j), cst(c, j), d_tmp(1)
!                   stop
                endif
                
                n = 0
                s = 1
                if (j == 1) then
                  s = 0
                  do p=1, 4
                    write (45+p, '(a,$)') "# (l,m)=  Total "
                    write (49+p, '(a,$)') "# l=  Total "
                  enddo
                endif
                do i = s, 1
                  !           write(*,'(a,$)') " * List of contributing (l,m): "
                  if (i == 1) then
                    do p=1, 8
                      write (45+p, *)
                      write (45+p, 555) enr(w, c, j)*ev, cst(c, j)
                    enddo
                  endif
                  do l = 0, lmax
                    do m = -l, l
                      d_tmp(1) = sum(csnlm(:,l,m))/3.d0
!                       if (d_tmp(1) /= 0.d0) then
                        if (i == 0) then
                          do p=1, 4
                            write (45+p, '("(",i0.3,",",i0.3,")"," ",$)') l, m
                          enddo
                        else
                          n = n + 1
                          write (46, 999) d_tmp(1)
                          do mu=1, 3
                            write (46+mu, 999) csnlm(mu,l, m)
                          enddo
                        endif
!                       endif
                    enddo
                    d_tmp(1) = sum(csnl(:,l))/3.d0
!                     if (d_tmp(1) /= 0.d0) then
                      if (i == 0) then
                        do p=1, 4
                          write (49+p, '(i3," ",$)') l
                        enddo
                      else
                        n = n + 1
                        write (50, 999) d_tmp(1)
                        dmax(l) = 100.d0*d_tmp(1)/cst(c, j)
                        
                        do mu=1, 3
                          write (50+mu, 999) csnl(mu,l)
                        enddo
                      endif
!                     endif
                  enddo
                enddo
                if (j /= 1 .and. n /= r) then
                  write (*, *) "Error! Inconsistent count of partial waves for energy ", j
                  stop
                endif
                r = n
              enddo
              call cwfile(8, 46, 1)
              call cwfile(8, 46, 0)
              deallocate (csnl)
              deallocate (csnlm)
            enddo

            write (*, '(a,i2,a)') " * Max contribution of partial waves: "
            do l = 0, lmax
              write (*, '(f7.3,a,$)') dmax(l), "%"
              if (dmax(l) > dmmax(l)) dmmax(l) = dmax(l)
            enddo
            write (*, *)
            write (*, *)

            if (testad) then
            
              call logfact   ! Need to be initialized to be able to use a3j

              allocate (cc(nener(c), 0:lmax))

              if (calalab .or. calapad) then

                do j = 1, nener(c)
                  call phasel(lmax, cc(j, :), -1.d0/dsqrt(enr(1, c, j)*2.d0))
                enddo

                allocate (dsr1(ndegb,3, 0:lmax, -lmax:lmax))
                allocate (mfpad(ndegb + 1, nener(c), nangx(1), nangx(2)))

                if (calalab) then
                  allocate(alm(ndegb+1,0:2,1))
                  allocate (btnk(nener(c), ndegb + 1))
                  s_tmp = "lab"
                endif
                if (calapad) then
                  allocate (tmpad(ndegb + 1, nang2(1), nang2(2)))
                  allocate (alm(ndegb,0:2*lmax, -2*lmax:2*lmax))
                  allocate (spha(0:2*lmax, -2*lmax:2*lmax))
                  s_tmp = "avg"
                endif

                do j = 1, nener(c)
                  call remtime(j,nener(c),initime1,crate,trim(s_tmp)//" AD ", rank)
!                   call remtime(j, nener(c), itime1, trim(s_tmp)//" AD ", rank)

                  nkm = 0
                  c_tmp(2) = 0.d0
                  c_tmp(3) = 0.d0
                  dsr1 = (0.d0, 0.d0)
                  do k = 1, ndegb

                    do s = 1, ntrans(k)
                      i = isyma(k, s)
                      d = idega(nkm + s)
                      mu = selrul(nkm + s, 2)
                      q = 0
                      do r = 1, np(nkm + s)
                        l = ilf(i, r)
                        c_tmp(1) = cc(j, l)*dcmplx(dip(nkm + s, j, (r - 1)*2 + 1), dip(nkm + s, j, (r - 1)*2 + 2))/y11
                        do p = 1, nmf(i, d, r)
                          q = q + 1
                          m = mmf(i, d, q)
                          call transdip(m, c_tmp(1)*bcf(i, d, q), dsr1(k, mu, l, -m), dsr1(k, mu, l, m))
                        enddo !p
                      enddo!r
                    enddo !s

                    call transdop(lmax, dsr1(k,:,:,:))
                    
                    nkm = nkm + ntrans(k)
                  enddo !k

                    ! Section to compute the angular distribution in the laboratory frame
                    if (calalab) then

                      call labal0(ndegb,lmax, dsr1, alm(:,:,1))
                      
                      do k =1, ndegb+1
                      
                      c_tmp(1) = alm(k,2,1)/alm(k,0,1)
                      if (dimag(c_tmp(1)) > eps) &
                        write (*, *) "Warning! Imaginary part of beta parameter is large: ", dimag(c_tmp(1))
                      btnk(j, k) = dble(c_tmp(1))
                      
                      enddo
                      
                      do k =1, ndegb

                      do it = 1, nang2(1)
                        d_tmp(1) = (1.d0 + 0.5d0*(3.d0*dcos(angr2(1, (it-1)*nang2(2)+1))**2 + 1.d0))*&
                                   btnk(j, k+1)*csnk(j, k)/(4.d0*pig)
                        do ip = 1, nangx(2)
                          mfpad(k+1, j, it, ip) = d_tmp(1)
                        enddo !ip
                      enddo !it
                      
                      enddo !k

                    endif ! callab

                    ! Section to compute the MFPADs for randomly oriented molecules
                    if (calapad) then
                      call avgalm(ndegb,lmax, dsr1, alm)
                      
                      ia = 1
                      do it = 1, nang2(1)
                        do ip = 1, nang2(2)
                          call SPH(2*lmax, angr2(1, ia), angr2(2, ia), spha, PIG)
                          c_tmp = (0.d0, 0.d0)
                          do k =1, ndegb
                          do l = 0, 2*lmax
                            do m = -l, l
                              c_tmp(k) = c_tmp(k) + alm(k, l, m)*spha(l, m)
                            enddo
                          enddo
                          
                          if (dimag(c_tmp(k)) > eps) write (*, *) "Warning! Imaginary part of avg mfpad is large: ", dimag(c_tmp(k))
                          tmpad(k, it, ip) = dble(c_tmp(k))*const3*enr(2, c, j)
                          enddo !k
                          ia = ia + 1
                        enddo !ip
                        
                        
                        if (intphi) then
                          do k =1, ndegb
                            call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                         ang2(2, :), tmpad(k, it, :), nang2(2), d_tmp(1))
                            mfpad(k + 1, j, it, 1) = d_tmp(1)
                          enddo
                        else
                          mfpad(2:ndegb+1, j, it, :) = tmpad(:, it, :)
                        endif

                      enddo !it

                    endif ! calapad
                    
                    do k =1, ndegb
                      mfpad(1, j, :, :) = mfpad(1, j, :, :) + mfpad(k + 1, j, :, :)
                    enddo

                enddo !j

                deallocate (dsr1)
                deallocate (alm)

                if (calalab) then
                  do w = 1, 2
                    call ofile(1, 48, (/"btnk"/), labs(c), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
                    write (48, *) "# Degeneracy= Total ", (k, k=1, ndegb)
                    do j = 1, nener(c)
                      write (48, *) enr(2, c, j)*ev, (btnk(j, k), k=1, ndegb + 1)
                    enddo
                    call cwfile(1, 48, 0)
                  enddo

                  deallocate (btnk)
                endif

                if (calapad) then
                  deallocate (tmpad)
                  deallocate (spha)
                endif

                if (prtpad > 1) then
                  call wmfpad(cphi, labs(c), (/labs(c), symb/), "mf", trim(scomp)//"_"//trim(s_tmp), &
                              ndegb + 1, nener(c), (/2*pig, 2*pig/), enr(1, c, :), ioner(c), vi, vf, nangx, ang2, mfpad)
                endif
                deallocate (mfpad)

              else

                ! Section to compute fixed in space angular distribution

                do j = 1, nener(c)
                  do l = 0, lmax
                    call phasex(l, sigl, -1.d0/dsqrt(enr(1, c, j)*2.d0))
                    cc(j, l) = zi**(-l)*exp(zi*sigl)
                  enddo
                enddo

                w = 2
                if (ioner(c) == 0.d0) w = 1

                if (prtpad > 0) then
                  allocate (phnp(nang2(1), nang2(2), 3))
                  allocate (cdnt(nang2(1)))
                  allocate (xtna(nang2(1)))
                  allocate (phol2(nang2(1), nang2(2)))

                  allocate (spha(0:lmax, -lmax:lmax))
                  allocate (tmpad(nener(c), nang2(1), nang2(2)))
                  allocate (mfpad(ndegb + 1, nener(c), nangx(1), nangx(2)))
                  allocate (ctnf(ndegb, nener(c), ntang1))
                endif
                if (calwtie) allocate (dpad(nang2(1), nang2(2)), &
                             wtpad(ndegb, nener(c), nangx(1), nangx(2)), wtnf(ndegb, nener(c), ntang1))
                if (calttie) then
                  allocate (enew(nener(c)))
                  ! We define a new energy grid with points separation as multipe of eir (IR frequency)
                  nemc = int(eir*nener(c)/(enr(1, c, nener(c)) - enr(1, c, 1)))
                  if (nemc == 0) nemc = 1
                  ! As the new grid can go over the interpolation range, we have to rest k
                  k = 0
                  do j = 1, nener(c)
                    enew(j) = enr(1, c, 1) + dble(j - 1)*eir/nemc
                    if (enew(j) > enr(1, c, nener(c))) k = k + 1
                  enddo
                  nenem = nener(c) - k
                  ideir = nint(eir/(enew(2) - enew(1)))
                  write (*, *) "* ideir, nenem = ", ideir, nenem
                  ddeir = 2*ideir
                  write (*, *) "* Old emin, emax, de = ", enr(1, c, 1), enr(1, c, nener(c)), enr(1, c, 2) - enr(1, c, 1)
                  write (*, *) "* New emin, emax, de = ", enew(1), enew(nenem), enew(2) - enew(1)
                  write (*, *)
                  if (dabs(enew(1 + ideir) - enew(1) - eir) > eps) then
                    write (*, *) "Warning! Inconsitent energy difference: eir = ", enew(1 + ideir) - enew(1), eir
                    write (*, *) "ttie calculation is disabled" 
                    calttie = .False.
                    deallocate (enew)
                  else
                    allocate (b2w(nener(c), 0:lmax, -lmax:lmax))
                    nemc = nener(c) - ddeir - k
                    write (*, *) "* nemc = ", nemc
                    allocate (tdnf(ndegb, nemc, ntang1, 2))
                    if (prtpad > 3) allocate (ttpad(ndegb, nemc, nangx(1), nangx(2)), mtpad(ndegb + 1, nemc, nangx(1), nangx(2)))
                  endif
                  
                endif
                
                if (prtddtp ) then
                  if ( v2 == v2min ) then
                    write (222) labs(c), ndegb
                    write (222) iir, v1, v2min, v2max
                    write (222) nener(c)
                    write (222) (enr(1, c, j), j=1, nener(c))
                  endif
                  write (222) ioner(c)
                endif

                allocate (csna(ndegb + 1, nener(c), ntang1))
                allocate (dsr1(3, nveca, ndega, npmax), dsr2(maxval(ntrans), npmax), dsrp(maxval(ntrans), npmax, nmfmax))
                csna = 0.d0
                do im = 1, ntang1
                  call setdcp(ang1(1, im), ang1(2, im), dcp)
                  call nbtocar_z(im, num1, 3)

                  if (prtpad > 0) then
                    call getfrax(im, ang1(:, im), ntang1, s_tmp)
                    mfpad = 0.d0
                  endif

                  if (calttie) call wignerd(0, ang1(1, im), ang1(2, im), wdm)

                  if (parelf) angr2(:, 1) = ang1(:, im)

                  if (calttie .and. prtpad > 3) mtpad = 0.d0
                  if (.not. prtxtie) call remtime(im,ntang1,initime1,crate,"FIS AD", rank)
!                   if (.not. prtxtie) call remtime(im, ntang1, itime1, "FIS AD", rank)
                  nkm = 0

                  do k = 1, ndegb

                    call nbtocar_z(k, num2, 1)
                    outname = trim(scomp)//"_"//trim(s_tmp)//".out"

                    if (prtpad > 0) then
                      phnp = 0.d0
                      phol2 = 0.d0
                    endif
                    if (calttie) b2w = (0.d0, 0.d0)

                    do j = 1, nener(c)
                      if (prtxtie) call remtime(j,nener(c),initime1,crate,"FIS AD "//trim(num1)//" "//trim(num2), rank)
!                       if (prtxtie) call remtime(j, nener(c), itime1, "FIS AD "//trim(num1)//" "//trim(num2), rank)

                      dsr1 = (0.d0, 0.d0)
                      dsr2 = (0.d0, 0.d0)
                      dsrp = (0.d0, 0.d0)
                      do s = 1, ntrans(k)
                        i = isyma(k, s)
                        d = idega(nkm + s)
                        mu = selrul(nkm + s, 2)
                        q = 0
!                      if ( abs(dcp(mu)) > eps ) then
                        do r = 1, np(nkm + s)
                          l = ilf(i, r)
                          dsr1(mu, i, d, r) = dcmplx(dip(nkm + s, j, (r - 1)*2 + 1), dip(nkm + s, j, (r - 1)*2 + 2))
                          dsr2(s, r) = dsr1(mu, i, d, r)*cc(j, l)*dcp(mu)
                          do p = 1, nmf(i, d, r)
                            q = q + 1
                            m = mmf(i, d, q)
                            dsrp(s, r, p) = dsr2(s, r)*bcf(i, d, q)
                          enddo
                        enddo
!                      endif
                      enddo

                      if (prtpad > 0) then
                        xtna = 0.d0
                        ia = 1
                        do it = 1, nang2(1)
                          do ip = 1, nang2(2)
                            call RSPH(lmax, angr2(1, ia), angr2(2, ia), spha, PIG)
                            ddiff = (0.d0, 0.d0)
                            do s = 1, ntrans(k)
                              i = isyma(k, s)
                              d = idega(nkm + s) !selrul(nkm+s,1)
                              mu = selrul(nkm + s, 2)
!                          if ( abs(dcp(mu)) > eps ) then
                              q = 0
                              do r = 1, np(nkm + s)
                                l = ilf(i, r)
                                do p = 1, nmf(i, d, r)
                                  q = q + 1
                                  m = mmf(i, d, q)
                                  ddiff = ddiff + spha(l, m)*dsrp(s, r, p)
                                enddo
                              enddo !r
!                          endif
                            enddo !s

                            ! Storing the MFPADs values
                            tmpad(j, it, ip) = (dreal(ddiff)**2 + dimag(ddiff)**2)*const2*enr(2, c, j)
                            if (prtddtp) write(222) dreal(ddiff), dimag(ddiff)
                            call rebatan(phol2(it, ip), phnp(it, ip, 3), dreal(ddiff), dimag(ddiff), 2)
!                             phnp(it, ip) = datan2(dimag(ddiff),dreal(ddiff))
!                        phnp(it,ip,3) = datan2(dimag(ddiff),dreal(ddiff))
                            ia = ia + 1
                          enddo !ip

                          call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                       ang2(2, :), tmpad(j, it, :), nang2(2), cdnt(it))
                          
                          ! Section to store the angularly resolved Wigner time delays
                          if (calwtie .and. j > 2) then
                          
                            dpad(it, :) = (phnp(it, :, 3) - phnp(it, :, 1))/deren(j)

                            if ( phasor ) then
                            call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                          ang2(2, :), dsin(tmpad(j-1, it, :))*dpad(it, :), nang2(2), d_tmp(1))
                            call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                          ang2(2, :), dcos(tmpad(j-1, it, :))*dpad(it, :), nang2(2), d_tmp(2))
                            xtna(it) = phasoradd(d_tmp(1), d_tmp(2), pig) !datan2(d_tmp(1), d_tmp(2))    ! Phasor addition
                            else
                            call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                          ang2(2, :), tmpad(j-1, it, :)*dpad(it, :), nang2(2), xtna(it))   ! Woerner
                            endif
                          endif
                          
                          phnp(it, :, 1) = phnp(it, :, 2)
                          phnp(it, :, 2) = phnp(it, :, 3)

                        enddo !it

                        if (intphi) then
                          mfpad(k + 1, j, :, 1) = cdnt
                          mfpad(1, j, :, 1) = mfpad(1, j, :, 1) + cdnt
                        else
                          mfpad(k + 1, j, :, :) = tmpad(j, :, :)
                          mfpad(1, j, :, :) = mfpad(1, j, :, :) + mfpad(k + 1, j, :, :)
                        endif

                        call fullint(doigl2(1), glst2(1), nigl2(1), nogl, glws, &
                                     ang2(1, :), cdnt*dsin(ang2(1, 1:nang2(1))), nang2(1), ctnf(k, j, im))

                        ! Section to integrate the Wigner time delays over the electron angles (Woerner)
                        if (calwtie .and. j > 2) then

                            if (intphi) then
                              if ( phasor ) then
                              wtpad(k,j-1,:,1)=xtna    ! Phasor addition
                              else
                              wtpad(k, j-1, :, 1) = xtna/cdnt     !Woerner
                              endif
                            else
                              wtpad(k, j-1, :, :) = dpad(:, :)
                            endif

                            if (ctnf(k, j-1, im) == 0.d0) then
                              wtnf(k, j-1, im) = 0.d0
                            else
                              if ( phasor ) then
                              call fullint(doigl2(1), glst2(1), nigl2(1), nogl, glws, &
                                           ang2(1, :), dsin(xtna)*dsin(ang2(1, 1:nang2(1))), nang2(1), d_tmp(1))
                              call fullint(doigl2(1), glst2(1), nigl2(1), nogl, glws, &
                                           ang2(1, :), dcos(xtna)*dsin(ang2(1, 1:nang2(1))), nang2(1), d_tmp(2))
                              wtnf(k, j-1, im) = phasoradd(d_tmp(1), d_tmp(2), pig) !datan2(d_tmp(1), d_tmp(2))    ! Phasor addition
                              else
                              call fullint(doigl2(1), glst2(1), nigl2(1), nogl, glws, &
                                           ang2(1, :), xtna*dsin(ang2(1, 1:nang2(1))), nang2(1), wtnf(k, j-1, im))
                              wtnf(k, j-1, im) = wtnf(k, j-1, im)/ctnf(k, j-1, im)     ! Woerner
                              endif
                            endif

                        endif
                        
!                         ctnf(k, j, im) = ctnf(k, j, im)*efac

                        ! Section to compute the two photon time delay coefficients
                        if (calttie) then
                          do w = -1, 1
!                         if ( dreal(wdm(w+2)) > eps .or. dimag(wdm(w+2)) > eps ) then
                            do u = 0, lmax
                              do v = -u, u
                                c_tmp(1) = (wdm(w + 2)/dsqrt(3.d0))*(-1)**(w + v + 1)       !dsqrt(4.d0/3.d0) from decomposition of dipole operator in spherical basis * 1/dsqrt(4.d0*pig) from the product of 3 spherical harmonics
                                do s = 1, ntrans(k)
                                  i = isyma(k, s)
                                  d = idega(nkm + s) !selrul(nkm+s,1)
                                  mu = selrul(nkm + s, 2)
!                                 if ( abs(dcp(mu)) > eps ) then
                                  q = 0
                                  do r = 1, np(nkm + s)
                                    l = ilf(i, r)
                                    c_tmp(2) = c_tmp(1)*a3j(u, 1, l, 0, 0, 0)*dsqrt((2*dble(u) + 1)*3*(2*dble(l) + 1))
                                    do p = 1, nmf(i, d, r)
                                      q = q + 1
                                      m = mmf(i, d, q)
                                      b2w(j, u, v) = b2w(j, u, v) + c_tmp(2)*a3j(u, 1, l, -v, w, m)*dsr2(s, r)
                                    enddo
                                  enddo
!                                 endif
                                enddo !s
                              enddo !u
                            enddo !v
!                         endif
                          enddo !w
                        endif

                      endif ! prtpad > 0

                      ! Section to compute cross section as a function of the polarization vector angle
                      ddiff = (0.d0, 0.d0)
                      do i = 1, nveca
                        do d = 1, ndega
                          do r = 1, npmax
                            do m = 1, 3
                              do n = 1, 3
                                ddiff = ddiff + dsr1(m, i, d, r)*dconjg(dsr1(n, i, d, r))*dcp(m)*dcp(n)
                              enddo
                            enddo
                          enddo
                        enddo
                      enddo
                      if (dimag(ddiff) > eps) write (*, *) "Warning! Imaginary part of csna is large: ", dimag(ddiff)
                      csna(k + 1, j, im) = dreal(ddiff)*const2*enr(2, c, j)
                      csna(1, j, im) = csna(1, j, im) + csna(k + 1, j, im)

                    enddo !j

                    nkm = nkm + ntrans(k)

                    if (calwtie) then
                      
!                       ! We differentiate after the integral (allowed by the Leibniz rule, i.e. integral limits a and b are constant)
!                       do it = 1, nangx(1)
!                         do ip = 1, nangx(2)
!                           call gradient(enr(1, c, :),wtpad(k, :, it, ip),nener(c),wtpad(k, :, it, ip),ddenr)
!                         enddo
!                       enddo
!                       call gradient(enr(1, c, :),wtnf(k, :, im),nener(c),wtnf(k, :, im),ddenr)
                      
                      wtpad(k, :, :, :) = wtpad(k, :, :, :)/atto2au
                      wtnf(k, :, im) = wtnf(k, :, im)/atto2au

                      if (prtxtie) then
                        do w = 1, 2
                          call ofile(1, 48, (/"wtie"/), symb(k), fstr(w), trim(outname), "-", ioner(c)*ev)
                          write (48, *) "# beta,alpha(deg)=", ang1(1, im)/deg2rad, ang1(2, im)/deg2rad
                          do j = 2, nener(c)-1
                            write (48, *) enr(w, c, j)*ev, wtnf(k, j, im), ctnf(k, j, im)*efac
                          enddo
                          call cwfile(1, 48, 0)
                        enddo
                      endif
                    endif

                    if (calttie) then

                      allocate (bbw(nenem, 0:lmax, -lmax:lmax, 2))
                      do l = 0, lmax
                        do m = -lmax, lmax
                          call interpogrid2(enr(1, c, 1:nener(c)), dreal(b2w(:, l, m)), nener(c), enew(:), bbw(:, l, m, 1), nenem)
                          call interpogrid2(enr(1, c, 1:nener(c)), dimag(b2w(:, l, m)), nener(c), enew(:), bbw(:, l, m, 2), nenem)
                        enddo
                      enddo

                      do j = 1, nemc
                        ddiff = 0.d0
                        do l = 0, lmax
                          do m = -lmax, lmax
                            ddiff = ddiff + dcmplx(bbw(j, l, m, 1), -bbw(j, l, m, 2))* &
                                    dcmplx(bbw(j + ddeir, l, m, 1), bbw(j + ddeir, l, m, 2))
                          enddo !l
                        enddo !m
                        tdnf(k, j, im, 1) = dreal(ddiff)
                        tdnf(k, j, im, 2) = dimag(ddiff)
                      enddo !j

                      if (prtxtie) then
                        do w = 1, 2
                          d_tmp(1) = 0.d0
                          call ofile(1, 48, (/"ttie"/), symb(k), fstr(w), trim(outname), "-", ioner(c)*ev)
                          write (48, *) "# beta,alpha(deg)=", ang1(1, im)/deg2rad, ang1(2, im)/deg2rad
                          do j = 1, nemc
                            call rebatan(d_tmp(1), xtia, tdnf(k, j, im, 1), tdnf(k, j, im, 2), 2)
                            xtia = xtia/(atto2au*2.d0*eir)
                            write (48, *) (enew(j + ideir) + ioner(c))*ev, xtia
                          enddo !j
                          call cwfile(1, 48, 0)
                        enddo
                      endif

                      if (prtpad > 3) then
                        allocate (opad(nang2(2)))
                        phol2 = 0.d0

!$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(initime2,ia,it,ip,spha,ddiff,l,m,u,v,phnp,d_tmp)
                        do j = 1, nemc
                          call remtime(j,nemc,initime2,crate,"ttpad", 0)
!                           call remtime(j, nemc, initime2, "ttpad", 0)
                          ia = 1
                          do it = 1, nang2(1)
                            do ip = 1, nang2(2)

                              call spharm(lmax, angr2(1, ia), angr2(2, ia), spha)

                              ddiff = (0.d0, 0.d0)
                              ddsum = (0.d0, 0.d0)
                              do l = 0, lmax
                                do m = -lmax, lmax
                                  ddiff = ddiff + spha(l, m)*dcmplx(bbw(j + ddeir, l, m, 1), bbw(j + ddeir, l, m, 2))
                                  ddsum = ddsum + spha(l, m)*dcmplx(bbw(j, l, m, 1), bbw(j, l, m, 2))
                                enddo
                              enddo

                              c_tmp(1) = ddiff*dconjg(ddsum)
                              phnp(1, ip, 1) = datan2(dimag(c_tmp(1)), dreal(c_tmp(1)))
!                            phnp(1,ip,1)=datan(dimag(c_tmp(1))/dreal(c_tmp(1)))
!                            call rebatan(phol2(it,ip),phnp(1,ip,1),dreal(c_tmp(1)),dimag(c_tmp(1)),1)

                              c_tmp(1) = ddiff + ddsum
                              opad(ip) = dreal(c_tmp(1))**2 + dimag(c_tmp(1))**2

                              ia = ia + 1
                            enddo !ip
                            if (intphi) then

                              call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                           ang2(2, :), phnp(1, :,1)*opad, nang2(2), d_tmp(1))
                                           
                              call fullint(doigl2(2), glst2(2), nigl2(2), nogl, glws, &
                                           ang2(2, :), opad, nang2(2), d_tmp(3))
                              opad(1) = d_tmp(3)
                              phnp(1,1,1)=d_tmp(1)/opad(1)
                              
                            endif
                            mtpad(k + 1, j, it, :) = opad(1:nangx(2))*const2*(enew(j + ideir + 1) + ioner(c))
                            mtpad(1, j, it, :) = mtpad(1, j, it, :) + mtpad(k + 1, j, it, :)
                            ttpad(k, j, it, :) = phnp(1, 1:nangx(2),1)/(atto2au*2.d0*eir)
                          enddo !it
                        enddo !j
!$OMP END PARALLEL DO
                        deallocate (opad)
                      endif !prtpad
                      deallocate (bbw)
                    endif !calttie

                  enddo !k
                  
                  if ( prtpad > 0 ) then
                      if (prtxtie) then
                        do w = 1, 2
                          call ofile(1, 48, (/"csie"/), labs(c), fstr(w), trim(outname), "-", ioner(c)*ev)
                          write (48, *) "# beta,alpha(deg)=", ang1(1, im)/deg2rad, ang1(2, im)/deg2rad
                          write (48, *) "# efac=", efac
                          write (48, *) "# Degeneracy= Total ", (k, k=1, ndegb)
                          do j = 2, nener(c) - 1
                            write (48, *) enr(w, c, j)*ev, sum(ctnf(1:ndegb, j, im)), (ctnf(k, j, im), k=1, ndegb)
                          enddo
                          call cwfile(1, 48, 0)
                        enddo
                      endif
                  endif
                  
                  if (prtpad > 1) mfkad(c, im, 1:nener(c), :, :) = mfpad(1, 1:nener(c), :, :)
                  if ( prtiad ) then
                  if (prtpad > 1) then
                    call wmfpad(cphi, labs(c), (/labs(c), symb/), "mf", trim(scomp)//"_"//trim(s_tmp), &
                                ndegb + 1, nener(c), ang1(:, im), enr(1, c, :), ioner(c), vi, vf, nangx, ang2, mfpad)
                  endif
                  if (prtpad > 2) then
                    if (prtxtie) then
                      if (calwtie) then
                        call wmfpad(cphi, labs(c), symb, "wt", trim(scomp)//"_"//trim(s_tmp), &
                                    ndegb, nener(c)-2, ang1(:, im), enr(1, c, 2:nener(c)-1), ioner(c), &
                                    vi, vf, nangx, ang2, wtpad(:,2:nener(c)-1,:,:))
                      endif
                      if (prtpad > 3) then
                        if (calttie) then
                          call wmfpad(cphi, labs(c), (/labs(c), symb/), "mt", trim(scomp)//"_"//trim(s_tmp), &
                                      ndegb + 1, nemc, ang1(:, im), enew(ideir + 1:nenem - ideir), ioner(c), &
                                      vi, vf, nangx, ang2, mtpad)
                          call wmfpad(cphi, labs(c), symb, "tt", trim(scomp)//"_"//trim(s_tmp), &
                                      ndegb, nemc, ang1(:, im), enew(ideir + 1:nenem - ideir), ioner(c), &
                                      vi, vf, nangx, ang2, ttpad)
                        endif
                      endif
                    endif
                  endif !prtpad
                  endif !prtiad

                enddo !im

                deallocate (dsr1, dsr2, dsrp)

                if (prtpad > 0) then
                  deallocate (spha)
                  deallocate (cdnt, phnp)
                  deallocate (xtna)
                  deallocate (phol2)
                  deallocate (tmpad)
                  deallocate (mfpad)
                endif
                if (calwtie) deallocate (dpad, wtpad)
                if (calttie) deallocate (b2w)
                if (calttie .and. prtpad > 3) deallocate (ttpad, mtpad)

                if (prtxtie) then

                  do w = 1, 2
                    call ofile(1, 50, (/"csna"/), labs(c), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
                    write (50,'(a,$)') "#  beta,alpha(deg)="
                    do im=1, ntang1
                      write (50, 444) ang1(1, im)/deg2rad,",",ang1(2, im)/deg2rad
                    enddo
                    write (50,*)
                    do j = 1, nener(c)
                      write (50, 777) enr(w, c, j)*ev, (csna(1, j, im), im=1, ntang1)
                    enddo
                    call cwfile(1, 50, 0)
                    do k = 1, ndegb
                      call ofile(1, 50, (/"csna"/), symb(k), fstr(w), trim(scomp)//".out", "-", ioner(c)*ev)
                      write (50,'(a,$)') "#  beta,alpha(deg)="
                      do im=1, ntang1
                        write (50, 444) ang1(1, im)/deg2rad,",",ang1(2, im)/deg2rad
                      enddo
                      write (50,*)
                      do j = 1, nener(c)
                        write (50, 777) enr(w, c, j)*ev, (csna(k + 1, j, im), im=1, ntang1)
                      enddo
                    enddo
                    call cwfile(1, 50, 0)
                  enddo

                else
                
                  allocate (mfmad(ndegb + 1, nener(c), nang1(1), nang1(2)))
                  im = 0
                  do i = 1, nang1(1)
                    do j = 1, nang1(2)
                      im = im + 1
                      mfmad(:, :, i, j) = csna(:, :, im)
                    enddo
                  enddo
                  s_tmp = "ma"
                  if (nang1(2) == 1) s_tmp = "mo"
                  call wmfpad(s_tmp, labs(c), (/labs(c), symb/), "mf", trim(scomp), &
                              ndegb + 1, nener(c), (/pig, 2.d0*pig/), enr(1, c, :), ioner(c), vi, vf, nang1, ang0, mfmad)
                  deallocate (mfmad)

                  if (prtpad > 0) then
                    ! Case where we have only one electron angle (then we gather by molecular angles)
                    if (ntang2 == 1) then
                      allocate (mfmad(ndegb + 1, nener(c), nang1(1), nang1(2)))
                      im = 0
                      do i = 1, nang1(1)
                        do j = 1, nang1(2)
                          im = im + 1
                          mfmad(2:, :, i, j) = ctnf(:, :, im)*efac
                          do k = 1, nener(c)
                            mfmad(1, k, i, j) = sum(ctnf(:, k, im))*efac
                          enddo
                        enddo
                      enddo
                      s_tmp = "oa"
                      if (nang1(2) == 1) s_tmp = "oo"
                      call wmfpad(s_tmp, labs(c), (/labs(c), symb/), "mf", trim(scomp), &
                                  ndegb + 1, nener(c), (/pig, 2.d0*pig/), enr(1, c, :), ioner(c), vi, vf, nang1, ang0, mfmad)
                      deallocate (mfmad)
                    endif
                  endif
                  
                  if (prtpad > 1) then
                    ! We compute the MFPAD integrated over the molecular angles
!                     d_tmp(1) = 0
!                     ! If beta is not starting at 0, consider a cone with solid angle centered at betamin+(betamax-betamin)/2
!                     if ( ang1(1, 1) /= 0.d0 ) then
!                       d_tmp(1) = ang1(1, 1)+(ang1(1, ntang1)-ang1(1, 1))/2.d0
!                     endif
                    
                    allocate (ctna(nang1(1)), mfmad(1, nener(c), nangx(1), nangx(2)))
                    do k = 1, ndegb
                      do j = 1, nener(c)
                        do it=1, nangx(1)
                          do ip=1, nangx(2)
                            ia = 0
                            do i = 1, nang1(1)
                              ia = (i - 1)*nang1(2) + 1
                              im = ia + nang1(2) - 1
                              if (nang1(2) == 1) then
                                ctna(i) = mfkad(c, ia, j, it, ip)
                              else
                                call fullint(doigl1(2), glst1(2), nigl1(2), nogl, glws, &
                                             ang1(2, ia:im), mfkad(c, ia:im, j, it, ip), nang1(2), ctna(i))
                              endif
!                               write(69, *) i, ang1(1,ia), ang2(1,it), ang2(2,ip), &
!                               dsin(ang1(1, ia)-d_tmp(1)), mfkad(c, ia:im, j, it, ip), ctna(i)
!                               write(*,*) ctna(i), ang1(1, ia)-d_tmp(1), dsin(ang1(1, ia)-d_tmp(1))
                              ctna(i) = ctna(i)*solang(i)
                            enddo !i
                            call fullint(doigl1(1), glst1(1), nigl1(1), nogl, glws, &
                                         ang1(1, 1:ntang1:nang1(2)), ctna, nang1(1), mfmad(1, j, it, ip))
!                             write(69, *) k, j, ang2(1,it), ang2(2,ip), ang1(1,ia), &
!                                          ctna(:), mfmad(1, j, it, ip)
                            mfmad(1, j, it, ip) = mfmad(1, j, it, ip)*solfac
                          enddo !ip
                        enddo !it
!                         stop
                      enddo !j
                    enddo !k
                    call wmfpad("i"//cphi(2:2), labs(c), (/labs(c), symb/), "mf", trim(scomp), &
                                1, nener(c), (/pig, 2.d0*pig/), enr(1, c, :), ioner(c), vi, vf, nangx, ang2, mfmad)
                    deallocate(ctna, mfmad)
                  endif


                  if (calwtie) then

                    allocate (xtna(nang1(1)), ctna(nang1(1)))
                    do k = 1, ndegb
                      allocate (ctia(nener(c)))
                      call ofile(1, 48, (/"_ep-"/), symb(k), "", "wtia"//trim(scomp)//".out", "-", ioner(c)*ev)
                      do j = 1, nener(c)
                        ia = 0
                        do i = 1, nang1(1)
                          ia = (i - 1)*nang1(2) + 1
                          im = ia + nang1(2) - 1
                          call fullint(doigl1(2), glst1(2), nigl1(2), nogl, glws, &
                                       ang1(2, ia:im), ctnf(k, j, ia:im), nang1(2), ctna(i))
                          call fullint(doigl1(2), glst1(2), nigl1(2), nogl, glws, &
                                       ang1(2, ia:im), wtnf(k, j, ia:im), nang1(2), xtna(i))    ! Woerner
!                             call fullint(doigl1(2), glst1(2), nigl1(2), nogl, glws, &
!                                        ang1(2, ia:im), wtnf(k, j, ia:im)*ctnf(k, j, ia:im), nang1(2), xtna(i))      ! Improved
                          ctna(i) = ctna(i)*dsin(ang1(1, ia))
                          xtna(i) = xtna(i)*dsin(ang1(1, ia))
                        enddo !i
                        call fullint(doigl1(1), glst1(1), nigl1(1), nogl, glws, &
                                     ang1(1, 1:ntang1:nang1(2)), ctna, nang1(1), ctia(j))
                        call fullint(doigl1(1), glst1(1), nigl1(1), nogl, glws, &
                                     ang1(1, 1:ntang1:nang1(2)), xtna, nang1(1), xtia)
                        ctia(j) = ctia(j)
                        xtia = xtia*mfac   ! Woerner
!                         xtia = xtia/ctia(j)   ! Improved
                        write (48, *) enr(2, c, j)*ev, xtia, ctia(j)
                      enddo !j
                      call cwfile(1, 48, 0)
                      deallocate (ctia)
                    enddo !k
                    deallocate (xtna, ctna)
                  endif ! calwtie

                  if (calttie) then
                    allocate (xtna(nang1(1)), ctna(nang1(1)))
                    do k = 1, ndegb
                      call ofile(1, 48, (/"_ep-"/), symb(k), "", "ttia"//trim(scomp)//".out", "-", ioner(c)*ev)
                      allocate (ctne(nemc))
                      call interpogrid2(enr(1, c, 1:nener(c)), csnk(:, k), nener(c), enew(ideir + 1:nener(c) - ideir), ctne, nemc)
                      d_tmp(3) = 0.d0
                      do j = 1, nemc
                        do p = 1, 2
                          ia = 0
                          do i = 1, nang1(1)
                            ia = (i - 1)*nang1(2) + 1
                            im = ia + nang1(2) - 1
                            call fullint(doigl1(2), glst1(2), nigl1(2), nogl, glws, &
                                         ang1(2, ia:im), tdnf(k, j, ia:im, p), nang1(2), xtna(i))
                            xtna(i) = xtna(i)*dsin(ang1(1, ia))
                          enddo !i
                          call fullint(doigl1(1), glst1(1), nigl1(1), nogl, glws, &
                                       ang1(1, 1:ntang1:nang1(2)), xtna, nang1(1), d_tmp(p))
                        enddo !p
                        call rebatan(d_tmp(3), xtia, d_tmp(1), d_tmp(2), 2)
                        xtia = xtia/(atto2au*2.d0*eir)
                        write (48, *) (enew(j + ideir) + ioner(c))*ev, xtia, ctne(j)
                      enddo !j
                      call cwfile(1, 48, 0)
                      deallocate (ctne)
                    enddo !k
                    deallocate (xtna, ctna)
                  endif !calttie

                endif ! prtxtie

                deallocate (csna)
                if (prtpad > 0) deallocate (ctnf)
                if (calwtie) deallocate (wtnf)
                if (calttie) deallocate (enew, tdnf)

              endif ! calalab .or. calapad
              
              deallocate(cc)
              
            endif ! testad
          endif ! testang

          if (testrul) deallocate (csnk)
          deallocate (csns)
          deallocate (csnp)

        enddo !v2

        if (testvib) deallocate (rgrid, ifvib, idvib)

        if (testrul) then
          deallocate (symb)
          deallocate (isym)
          deallocate (isyma)
          deallocate (idega)
          deallocate (idegb)
          deallocate (nnmka)
          deallocate (selrul)
          deallocate (ntrans)
        endif

        if (testvib) deallocate (evib, fvib)
        
        deallocate(edp)
        deallocate (dip)
        deallocate (mat)
        deallocate (np)

        deallocate (wsym, ilig, icol)
        deallocate (dipname)
        deallocate (eions)

      enddo !c
      close (78)

      if (nvecb > 1 .and. .not. testvib) then
        write (*, *) "* Writing total cross section... "

        nemc = nener(1)
        allocate (enew(nemc), ies(nvecb))
        do w = 1, 2
          emm = 1.d15
          do c = 1, nvecb
            d_tmp(1) = minval(enr(w, c, 1:nener(c)))
            if (d_tmp(1) < emm(1)) emm(1) = d_tmp(1)

            d_tmp(1) = maxval(enr(w, c, 1:nener(c)))
            if (d_tmp(1) < emm(2)) emm(2) = d_tmp(1)
          enddo

          do j = 1, nemc
            enew(j) = emm(1) + dble(j - 1)*(emm(2) - emm(1))/dble(nemc - 1)
          enddo

          allocate (csnc(nvecb, nemc))
          csnc = 0.d0
          do c = 1, nvecb
            j = 0
            d_tmp(1) = minval(enr(w, c, 1:nener(c)))
            do while (j < nemc)
              j = j + 1
              if (enew(j) >= d_tmp(1)) exit
            enddo
            ies(c) = j
            if (ies(c) == nemc) then
              write (*, *) "Warning! Ip is out of energy range for symmetry ", trim(labs(c))
            else
              call interpogrid2(enr(w, c, 1:nener(c)), cst(c, 1:nener(c)), nener(c), &
                                enew(ies(c):nemc), csnc(c, ies(c):nemc), nemc - ies(c) + 1)
            endif
          enddo

          open (50, file="tot"//fstr(w)//"csnc.out")
          write (50, *) "# Photoionization_channels= Total ", (trim(labs(c))//" ", c=1, nvecb)
          do j = 1, nemc
            write (50, 777) enew(j)*ev, sum(csnc(:, j)), (csnc(c, j), c=1, nvecb)
          enddo
          close (50)

          if (testrul) then
            allocate (csnl(nemc, 3))
            csnl = 0.d0
            do l = 1, 3
              open (50, file="tot"//fstr(w)//"csnc"//"_"//dipc(l)//".out")
              write (50, *) "# Photoionization_channels= Total ", (trim(labs(c))//" ", c=1, nvecb)
              csnc = 0.d0
              do c = 1, nvecb
                if (ies(c) /= nemc) &
                  call interpogrid2(enr(w, c, 1:nener(c)), csnt(c, 1:nener(c), l), nener(c), &
                                    enew(ies(c):nemc), csnc(c, ies(c):nemc), nemc - ies(c) + 1)
                csnl(:, l) = csnl(:, l) + csnc(c, :)
              enddo
              do j = 1, nemc
                write (50, 777) enew(j)*ev, sum(csnc(:, j)), (csnc(c, j), c=1, nvecb)
              enddo
              close (50)
            enddo

            open (50, file="tot"//fstr(w)//"csno"//".out")
            write (50, *) "# Orientations= Average x y z"
            do j = 1, nemc
              write (50, 777) enew(j)*ev, sum(csnl(j, :))/3.d0, csnl(j, 3), csnl(j, 1), csnl(j, 2)
            enddo
            close (50)
            deallocate (csnl)

          endif
          deallocate (csnc)
        enddo

        if (prtpad > 1 .and. prtiad) then
          write (*, *) "* Writing total MFPAD... "
          allocate (mfpad(1, nemc, nangx(1), nangx(2)), opad(nemc))
          do im = 1, ntang1
            call getfrax(im, ang1(:, im), ntang1, s_tmp)
            mfpad = 0.d0
            do c = 1, nvecb
              if (ies(c) /= nemc) then
                do it = 1, nangx(1)
                  do ip = 1, nangx(2)
                    opad = 0.d0
                    call interpogrid2(enr(2, c, 1:nener(c)), mfkad(c, im, 1:nener(c), it, ip), nener(c), &
                                      enew(ies(c):nemc), opad(ies(c):nemc), nemc - ies(c) + 1)
                    mfpad(1, :, it, ip) = mfpad(1, :, it, ip) + opad(:)
                  enddo
                enddo
              endif
            enddo
            call wmfpad(cphi, "total", "total", "mf", trim(scomp)//"_"//trim(s_tmp), &
                        1, nemc, ang1(:, im), enew(:) - emm(1), emm(1), vi, vf, nangx, ang2, mfpad)
          enddo
          deallocate (mfpad, opad)
        endif

        deallocate (enew, ies)
      endif

      if (testang) then
        write (*, '(a)') " * Max max contribution of partial waves: "
        do l = 0, lmax
          write (*, '(f7.3,a,$)') dmmax(l), "%"
        enddo
        write (*, *)
        write (*, *)

        deallocate (nla, ndg, npa)
        deallocate (ilf, ihf, nmf, bcf, mmf)
        if (testad) then
          if (ntang1 == 0) deallocate (ang0,solang)
          if (.not. calapad .and. .not. calalab) deallocate (ang1)
          deallocate (ang2, angr1, angr2)
        endif
        deallocate (dmax, dmmax)
      endif

      deallocate (cst, enr, deren, ioner)
      if (testrul) deallocate (csnt)
      if (prtpad > 1) deallocate (mfkad)
      deallocate (nener, labs)
      if ( dointe ) deallocate(egrid)

      close (17)
      close (37)
      if (prtddtp) close (222)

    end

    subroutine angpar(str, angmin, angmax, nigl0, nogl, yang, pig, doigl, nigl, nang, ntang)

      implicit none

      character*(*), intent(in) :: str
      integer, intent(in) :: nigl0, nogl
      double precision, intent(in) :: yang, pig, angmin(2), angmax(2)
      logical, intent(inout) :: doigl(2)
      integer, intent(inout) :: nang(2)
      integer, intent(out) :: nigl(2), ntang

      integer i

      do i = 1, 2
!         read (57, *) nang(i), angmin(i), angmax(i)
        nigl(i) = ceiling(nigl0*(angmax(i) - angmin(i))/yang)
        if (nang(i) == -1) then
          doigl(i) = .True.
          nang(i) = nigl(i)*nogl
        endif
      enddo
      write (*, *) "* # of "//trim(str)//" angles: ", nang
      write (*, *) "* # of GL intervals: ", nigl
      ntang = nang(1)*nang(2)

      return

    end subroutine angpar

    subroutine angrid(np, ip, xmin, xmax, grid)

      implicit none

      integer, intent(in) :: np(2), ip
      double precision, intent(in) :: xmin, xmax
      double precision, intent(out) :: grid(np(1)*np(2))

      integer i, j, k
      double precision stp

      if (ip == 1) then
        if (np(1) == 1) then
          grid = xmax
        else
          stp = (xmax - xmin)/dble(np(1) - 1)
          k = 0
          do i = 1, np(1)
            do j = 1, np(2)
              k = k + 1
              grid(k) = xmin + dble(i - 1)*stp
            enddo
          enddo
        endif
      else
        if (np(2) == 1) then
          grid = xmax
        else
          stp = (xmax - xmin)/dble(np(2) - 1)
          k = 0
          do i = 1, np(1)
            do j = 1, np(2)
              k = k + 1
              grid(k) = xmin + dble(j - 1)*stp
            enddo
          enddo
        endif
      endif

    end subroutine angrid

    subroutine setdcp(beta, alpha, dcp)

      implicit none

      double precision, intent(in) :: beta, alpha
      double precision, intent(out) :: dcp(3)

      dcp(1) = dsin(beta)*dsin(alpha)
      dcp(2) = dcos(beta)
      dcp(3) = dsin(beta)*dcos(alpha)

    end subroutine setdcp

    subroutine ofile(nfile, ifile, tabstr, prestr, fstr, sufstr, tstr, ip)

      implicit none

      integer, intent(in) :: nfile, ifile
      double precision, intent(in) :: ip
      character*(*), intent(in) :: tabstr(nfile), prestr, fstr, sufstr, tstr
      integer i, j, k

      do i = 1, nfile
        j = ifile + i - 1
        open (j, file=trim(prestr)//trim(fstr)//trim(tabstr(i))//trim(sufstr))
        if (trim(tstr) == "-") then
          write (j, *) "# ip(eV)=", ip
        else
          write (j, *) "# ip(eV)=", ip
          write (j, *) "# "//trim(tstr)
        endif
      enddo

    end subroutine ofile

    subroutine cwfile(nfile, ifile, mode)

      implicit none

      integer, intent(in) :: nfile, ifile, mode
      integer i, j

      do i = 1, nfile
        j = ifile + i - 1
        if (mode == 0) close (j)
        if (mode == 1) write (j, *)
      enddo

    end subroutine cwfile

    subroutine wmfpad(cphi, lab, labs, atyp, suf, nk, nener, ang1, ener, ioner, vi, vf, nang2, ang2, mfpad)

      implicit none

      integer, intent(in) :: nk, nener, nang2(2)
      double precision, intent(in) :: ang1(2), ioner, vi(3), vf(3), ener(nener)
      double precision, intent(in) :: ang2(2, maxval(nang2)), mfpad(nk, nener, nang2(1), nang2(2))
      character*(*), intent(in) :: cphi, lab, labs(nk), atyp, suf

      integer i, j, k, n, it, ip
      character*10 num
      character*100 rname
      double precision pig, ev, deg2rad
      parameter(pig=4.d0*datan(1.d0), eV=27.2113845d0, deg2rad=pig/180.d0)

      rname = trim(atyp)//trim(cphi)//"d"//trim(suf)
      
      if (nang2(2) /= 1) then
        rname = trim(lab)//"_"//trim(rname)//"-n"

        do j = 1, nener
          n = j
          if (atyp == "wt" .or. atyp == "tt") n = n + 1
          call nbtocar_z(n, num, 3)
          open (50, file=trim(rname)//trim(num)//".out")
          write (50, *) 0, ang1(1), ang1(2), ener(j), ioner
          write (50, '(3F8.4)') (vi(k), k=1, 3)
          write (50, '(3F8.4)') (vf(k), k=1, 3)
          write (50, *) nang2(1), nang2(2), nk
          write (50, *) (0.d0, k=1, nk)
          write (50, *) (0.d0, k=1, nk)
          write (50, *) (ang2(1, it), it=1, nang2(1))
          write (50, *) (ang2(2, ip), ip=1, nang2(2))
          do it = 1, nang2(1)
            do ip = 1, nang2(2)
              write (50, *) (mfpad(k, j, it, ip), k=1, nk)
            enddo
          enddo
          close (50)
        enddo

      else
        rname = trim(rname)//".out"
        do k = 1, nk
          open (50, file=trim(labs(k))//"_ep-"//trim(rname))
          write (50, *) "# ip(eV)=", ioner*ev
          write (50, *) "# beta,alpha(deg)=", ang1(1)/deg2rad, ang1(2)/deg2rad
          write (50, '(a,2000f8.3)') "# Ep(eV)=", ((ener(j) + ioner)*ev, j=1, nener)
          do it = 1, nang2(1)
            write (50, *) ang2(1, it), (mfpad(k, j, it, 1), j=1, nener)
          enddo
          close (50)
        enddo
      endif

    end subroutine wmfpad

    subroutine getfrax(im, ang1, ntang1, cstr)

      implicit none

      integer, intent(in) :: im, ntang1
      double precision, intent(in) :: ang1(2)
      character*(*), intent(out) :: cstr

      character*10 num
      double precision pig
      parameter(pig=4.d0*datan(1.d0))

      call nbtocar_z(im, num, 3)
      cstr = "f"//trim(num)
      if (ntang1 <= 3) then
        if (ang1(1) == 0.d0) then
          if (ang1(2) == 0.d0) cstr = "z"
        endif
        if (ang1(1) == pig/2.d0) then
          if (ang1(2) == 0.d0) cstr = "x"
          if (ang1(2) == pig/2.d0) cstr = "y"
        endif
      endif

    end subroutine getfrax

!     transform S-matrix normalized dipoles from the X angular basis (DSPM)
!     to the standard Ylm basis (DLM)
    subroutine transdip(mf, dxyz, dylm, dylp)

      implicit none
      integer, intent(in) :: mf
      complex*16, intent(in) :: dxyz
      complex*16, intent(inout) :: dylm, dylp

      complex*16 aa, zi
      double precision insq2
      parameter(zi=(0.d0, 1.d0), insq2=1.d0/dsqrt(2.d0))

      if (mf == 0) then
        dylm = dylm + dxyz
      elseif (mf > 0) then
        aa = dxyz*insq2
        dylp = dylp + (-1)**mf*aa
        dylm = dylm + aa

      else
        aa = dxyz*insq2*zi
        dylm = dylm - (-1)**mf*aa
        dylp = dylp + aa
      endif

      return

    end subroutine transdip

!     transform  dipole operator components from the X angular basis
!     to the standard Ylm basis
    subroutine transdop(lmax, dylm)

      implicit none
      integer, intent(in) :: lmax
      complex*16, intent(inout) :: dylm(3, 0:lmax, -lmax:lmax)

      integer l, m
      complex*16 aa, zi
      double precision insq2
      parameter(zi=(0.d0, 1.d0), insq2=1.d0/dsqrt(2.d0))

      do l = 0, lmax
        do m = -l, l
          aa = -(dylm(3, l, m) + zi*dylm(1, l, m))*insq2
          dylm(1, l, m) = (dylm(3, l, m) - zi*dylm(1, l, m))*insq2
          dylm(3, l, m) = aa
        enddo
      enddo

      return

    end subroutine transdop
    
    
    double precision function phasoradd(num, den, pig)
    
    implicit none
    double precision, intent(in) :: num, den, pig
    
    if ( den == 0.d0 ) then
      phasoradd = sign(1.d0,num)*pig/2.d0
    else
      phasoradd = datan2(num,den)
      if ( den < 0.d0 ) phasoradd = phasoradd+pig
    endif
    
    return
    
    end function phasoradd
