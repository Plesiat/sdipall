# Readme

## About sdipall

**sdipall** is a Fortran code that can compute single photoionization partial cross section and Wigner time delays from dipole matrix elements.
When angular and vibrational information are provided, sdipall can compute fully differential magnitudes:

* Vibrationally resolved molecular frame angular distribution (vMFPAD):

```math
\frac{d^2\sigma^{v v'}_{\alpha \mu'} \left(\omega\right)}{d\Omega_M d\Omega_e}=
%\pi\left(\frac{e^{2}}{\alpha_s\omega}\right)^2 \left(\frac{\alpha_{s}^{3}}{e^{4}}\frac{4}{3}\pi\right)
\frac{4\pi^{2}\omega}{3c}
\left|\sum_{p,q,h,l,\mu}{
(-i)^l e^{i\sigma_l\left(\omega\right)}\mathcal{D}^{1}_{\mu\mu'}\left(\Omega_M\right)T^{v v' -}_{\alpha p q h l \mu} \left(\omega\right)X_{p q h l}\left(\Omega_e\right)}\right|^2
```

* Vibrationally resolved Wigner time delays:

```math
\frac{d^2\tau^{vv'}_{\alpha\mu'}\left(\omega\right)}{d\Omega_M d\Omega_e}  =  
 \frac{\partial}{\partial \varepsilon} \arg \left(\sum_{p,q,h,l,\mu}\left(-i\right)^{l}e^{i\sigma_l\left(\omega\right)}D^1_{\mu \mu'}\left(\Omega_M\right)T^{v v' -}_{\alpha p q h l \mu} \left(\omega\right)X_{p q h l}\left(\Omega_e\right)\right)
```

where the vibrationally resolved dipole matrix elements are:

```math
T^{v v' -}_{\alpha p q h l \mu} \left(\omega\right)=\left<\varphi^{-}_{\varepsilon p q h l}\left(r,R\right)\chi_{f_{\alpha},v'}\left(R\right)\left|rY^{\mu}_{1}\left(\theta,\phi\right)
\right| \varphi_\alpha\left(r,R\right) \chi_{i,v}\left(R\right)\right>
```

$`\alpha`$ refers to the inital molecular orbital from which the electron is emitted, $`v`$ and $`v'`$ are respectively the initial and final vibrational quantum numbers, $`\Omega_e=\left(\theta,\phi\right)`$ correspond to the angles of the photoelectron in the molecular frame and $`\Omega_M=\lbrace\alpha,\beta,\gamma\rbrace`$ to the Euler's angles between the molecular frame and the photon frame.

$`\omega`$ is the photon energy which is related to the photoelectron energy $`\varepsilon`$ through the equation $`\omega=IP^{v,v'}_{\alpha}+\varepsilon`$, where $`IP^{v,v'}_{\alpha}`$ is the ionization energy required to create a hole in the $`\alpha`$ molecular orbital.

$`p`$ denotes the irreducible representation (IR) of the molecular point group under consideration, $`q`$ stands for a component of this representation if its dimensionality is greater than one, $`h`$ distinguishes between different bases of the same IR corresponding to the same value of the angular momentum of the escaping electron $`l`$ and $`\mu'`$ ($`\mu`$) corresponds to the photon state in the molecular (laboratory) frame of reference.

$`\sigma_l\left(\omega\right)`$ corresponds to the Coulomb phase shift, $`\mathcal{D}^{1}_{\mu\mu'}`$ is a Wigner rotation matrix with $`(l=1,m=\mu,m'=\mu')`$ and $`X_{p q h l}\left(\Omega_e\right)`$ is a symmetry adaptated real spherical harmonics defined at the center of mass of the molecule.

$`\varphi_\alpha`$ is the initial molecular orbital, $`\varphi^-_{\varepsilon p q h l}`$ the final electronic continuum state of the photoelectron with energy $`\varepsilon`$ in the $`(pqhl)`$-th channel, $`\chi_{i,v}`$ is the initial vibrational state, $`\chi_{f_{\alpha},v'}`$ is the final vibrational state, $`Y^{\mu}_{1}`$ is the spherical harmonics with $`(l=1,m=\mu)`$, $`r`$ the coordinates of the electron and $`R`$ the normal coordinates of the vibrational mode.

**Example of a vMFPAD obtained using sdipall:**
![](examples/mfpad_ch4+c1s.png)

## Installation

Requirements:
1. ifort or gfortran compilers
2. make

Compilation:

    make

## Inputs

* sdipall.inp (required): Contains the filenames of the dipole matrix elements and the ionization potentials of each photoionization channel

* selrul.inp (optional): Contains the selection rules information corresponding to each filename previously defined (dipole transition)

* symang.inp (optional): Contains the angular information corresponding to the one center spherical basis expansion and to the polarization vector and photoelectron

<table>
  <tr>
    <th colspan="3", style="text-align: center">Details of symang.inp</th>
  </tr>
  <tr>
    <td colspan="3">Maximum number of partial waves, maximum number of terms in the expansion (among all the final symmetries)</td>
  </tr>
  <tr>
  <td colspan="3">Copy paste of the spherical basis expansion as given by lcaompi</td>
  </tr>
  <tr>
    <td colspan="3">Number of molecular angles</td>
    </tr>
  <tr>
    <td>If = -1:</td>
    <td>If = 0:</td>
    <td>If > 0:</td>
  </tr>
  <tr>
    <td><span style="color:gray">Skip</span></td>
    <td>Number of molecular polar angles, min angle, max angle</td>
    <td>Polar angle 1, azimuth angle 1</td>
  </tr>
  <tr>
    <td><span style="color:gray">Skip</span></td>
    <td>Number of molecular azimuth angles, min angle, max angle</td>
    <td> ... </td>
  </tr>
  <tr>
    <td colspan="3">Number of electronic polar angles (if = 0: wtie and wtia will not be computed), min angle, max angle</td>
   </tr>
   <tr>
    <td colspan="3" >Number of electronic azimuth angles (if = 0: wtie and wtia will not be computed), min angle, max angle</td>
   </tr>
   <tr>
   <td colspan="3">Printing option (0=print only csna, 1=print wtie, wtia, ttie, ttia, 2=print mfpad, 3=print wtpad, 4=print ttpad), electronic azimuth angle integration option (T/F), projection option (T/F)</td>
   </tr>
   <tr>
   <td colspan="3">If projection true:</td>
   </tr>
   <tr>
   <td colspan="3">x, y, z (coordinates of the initial vector, to be aligned)</td>
   </tr>
   <tr>
   <td colspan="3">x, y, z (coordinates of the final vector, to be aligned with)</td>
   </tr>
</table>

* vibmod.inp (optional): Contains the filenames of the vibrational wavefunctions and energies of the initial and final states

## Licence

This code is under GNU General Public License (GPL) version 3
