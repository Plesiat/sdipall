!     -----------------------------------------------------------------
      SUBROUTINE PHASEX (L, PHI, ETA)
!     -----------------------------------------------------------------
      IMPLICIT double precision (A-H, O-Z)
      dimension carg(2), cans(2)
      real error
      error = 1.e-13
      carg(1) = L + 1
      carg(2) = ETA
      lfo = 1
      call cdlgam(carg, cans, error, lfo)
      phi = atan2(cans(2), cans(1))
      END subroutine phasex

      
      
!     -----------------------------------------------------------------
      SUBROUTINE PHASEL(LMAX, CC, ETA)
!     -----------------------------------------------------------------
      
      IMPLICIT double precision (A-H, O-Z)

      COMPLEX*16 PHASE, ZI, ZL, CC
      DIMENSION CC(0:LMAX)
!   
!     CC(L) = (-i)**L * EXP ( i * ( arg(GAMMA( 1 + L + i*ETA)) -
!                         arg(GAMMA( 1 +     i*ETA)) )
!
      CC = 0.d0
!write(*,*) ' ETA = ' , eta
      PHASE = 1.d0
      PHI   = 0.d0
      ZL    = 1.d0
      ZI    = (0.d0, -1.d0)     ! -i

      DO L = 0, LMAX
      XL = L
      CC(L) = ZL * PHASE
      ZL = ZL * ZI                     ! (-i)**L
      PHI = PHI + ATAN2( ETA, XL+1 )   ! sigma_L
      CPHI =  COS( PHI )
      SPHI =  SIN (PHI )
      PHASE = DCMPLX( CPHI , SPHI)     ! exp( i sigma_L)
!  write(*,*) 'atan, phi, cphi, sphi ', atan2(eta, xl+1), phi, cphi, sphi
      ENDDO
!     do l = 0,lmax
!       write(*,*) ' L, CC ', L, CC(L)
!     enddo
      END subroutine phasel
