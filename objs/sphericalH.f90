


!     -----------------------------------------------------------------
      subroutine plm(la, cta, sta, p, maxl) 
!-----------------------------------------------
!   M o d u l e s 
!-----------------------------------------------
      
!     -----------------------------------------------------------------
!
!     Computes the normalized associated Legendre polynomial Plm(cta),
!     for all l,m up to la,la , factor (-1)**m removed
!     employs normalized recursion, to avoid overflows for high l values
!     tested up to la = 200
!
!     to add phase factor, activate the 2 cphas lines in place of the
!     preceeeding ones
!
!...Translated by Pacific-Sierra Research 77to90  4.3C  15:06:20   2/16/04  
!...Switches: -xf -plov -rflnrux2 
!**************************************************************
!                                                             *
! subroutines and functions called from this routine          *
!                                                             *
! no subroutines or functions called from this program unit   *
!                                                             *
!**************************************************************
      implicit none
!-----------------------------------------------
!   D u m m y   A r g u m e n t s
!-----------------------------------------------
      integer , intent(in) :: la 
      integer , intent(in) :: maxl 
      double precision , intent(in) :: cta 
      double precision , intent(in) :: sta 
      double precision , intent(inout) :: p(0:maxl,0:maxl) 
!-----------------------------------------------
!   L o c a l   V a r i a b l e s
!-----------------------------------------------
      integer :: l, m 
      double precision :: al1, al2, pl1, almm, almp 
!-----------------------------------------------

      p(0,0) = 1/sqrt ( 2.D0 )  
      if (la > 0) then 
         p(1,0) = cta*sqrt ( 3.D0/2.D0 )  
         p(1,1) = sta*sqrt ( 3.D0/4.D0 )  
!phas P(1,1) = -STA * sqrt(3.d0/4.d0)
         do l = 2, la 
 
            al1      = l + l + 1 
            al2      = l + l 
            pl1      = p(l-1,l-1)*sqrt ( al1/al2 )  
            p(l,l)   = pl1*sta 
!phas   P(L,L)   =-PL1 * STA
!             write(*,*) "kk", la, cta, sta, maxl, pl1, cta, al2
            p(l,l-1) = pl1*cta*sqrt ( al2 )  
 
            do m = 0, l - 2 
               almm   = l - m 
               almp   = l + m 
               p(l,m) = cta*p(l-1,m)*sqrt ( al1* ( al1 - 2 ) / ( almm*almp ) &
                   )  - p(l-2,m)*sqrt ( al1* ( almp - 1 ) * ( almm - 1 ) / ( &
                   ( al1 - 4 ) *almm*almp )  )  
            end do 
 
         end do 
      endif 
      return  
      end subroutine plm 



    subroutine RSPH(lmax,theta,phi,spha,PIG)
	
	implicit none
	
	integer l, m, lmax
	double precision d_fact
	double precision theta, phi, cth, sth, cph, sph, PIG
	double precision PLG(0:LMAX,0:LMAX), CSMPHI(-LMAX:LMAX)
	complex*16 spha(0:lmax,-lmax:lmax)
	
	CTH = COS(theta)
    STH = SIN(theta)
    CPH = COS(phi)
    SPH = SIN(phi)
	CALL PLM (LMAX, CTH, STH, PLG, LMAX)
    CALL CSPHI1 (LMAX, CPH, SPH, CSMPHI)
!     write(*,*) "plg:"
!     do l=0, lmax
!       write(*,*) l, plg(l,0:l)
!     enddo
!     write(*,*) "csmphi:", CSMPHI
!     write(*,*) cth, sth, cph, sph
!     write(*,*)
    
    
    do l=0, lmax
      do m=-l, l
!         write(*,*) l, m, plg(l,abs(m)), CSMPHI(m)
        spha(l,m) = plg(l,abs(m))*CSMPHI(m)
      enddo
    enddo
!     write(*,*) "spha:", spha(6,-6:6)
!     stop
    
    end
    
    
    subroutine SPH(lmax,theta,phi,spha,PIG)
	
	implicit none
	
	integer l, m, lmax
	double precision d_fact
	double precision theta, phi, csta, d_tmp, PIG, plgndr
	complex*16 spha(0:lmax,-lmax:lmax)
	
	csta=dcos(theta)
	do l=0,lmax
		do m=0,l
! 			write(*,*) l,m,(2.d0*l+1.d0),(d_fact(l-m)),(d_fact(l+m)), &
! 	(2.d0*l+1.d0)*(d_fact(l-m))/(d_fact(l+m))*((d_fact(l+m))/(d_fact(l-m)))*(-1.d0)**(-m)
			d_tmp=plgndr(l,m,csta)*dsqrt(((2.d0*l+1.d0)/(4.d0*PIG))*(d_fact(l-m))/(d_fact(l+m)))
			spha(l,m)=dcmplx(d_tmp*dcos(m*phi),d_tmp*dsin(m*phi))
! 			d_tmp=d_tmp*((d_fact(l-m))/(d_fact(l+m)))*(-1.d0)**(m)
			d_tmp=d_tmp*(-1.d0)**(m)
			spha(l,-m)=dcmplx(d_tmp*dcos(-m*phi),d_tmp*dsin(-m*phi))
		enddo
	enddo

	end


      double precision FUNCTION PLGNDR(L,M,X)
      integer l,m
      integer i,ll
      double precision facto,pll,pmm,pmmp1,somx2
      double precision, intent(in) ::  x
      IF(M.LT.0.OR.M.GT.L.OR.ABS(X).GT.(1.0d0)) THEN
		write(*,*)
		write(*,*) "Error!!! - l and m are not corrects!"
		write(*,*) M, L, X
		write(*,*)
		stop
      ENDIF
      PMM=1.
      IF(M.GT.0) THEN
        SOMX2=SQRT((1.-X)*(1.+X))
        FACTO=1.
        DO 11 I=1,M
          PMM=-PMM*FACTO*SOMX2
          FACTO=FACTO+2.
11      CONTINUE
      ENDIF
      IF(L.EQ.M) THEN
        PLGNDR=PMM
      ELSE
        PMMP1=X*(2*M+1)*PMM
        IF(L.EQ.M+1) THEN
          PLGNDR=PMMP1
        ELSE
          DO 12 LL=M+2,L
            PLL=(X*(2*LL-1)*PMMP1-(LL+M-1)*PMM)/(LL-M)
            PMM=PMMP1
            PMMP1=PLL
12        CONTINUE
          PLGNDR=PLL
        ENDIF
      ENDIF
      RETURN
      END

      subroutine lplm(lmax, cth, p)
      
      implicit none
      
      integer, intent(in) :: lmax
      double precision, intent(in) :: cth
      double precision, intent(inout) :: p(0:lmax,-lmax:lmax)
      
      integer l, m
      
      double precision PLGNDR, d_fact
      
      do l=0, lmax
        p(l,0)=PLGNDR(l,0,cth)
        do m=1, l
          p(l,m)=PLGNDR(l,m,cth)
          p(l,-m)=(d_fact(l-m)/d_fact(l+m))*p(l,m)*(-1)**m
        enddo
      enddo
      
      end subroutine lplm
      
      
      subroutine spharm(lmax,theta,phi,spha)
      
      implicit none
      
      integer, intent(in) :: lmax
      double precision, intent(in) :: theta, phi
      complex*16, intent(inout) :: spha(0:lmax,-lmax:lmax)
      
      integer l, m
      double precision cth
      double precision lgp(0:lmax,-lmax:lmax)
      double precision fac, den, rep, pig
      double precision d_fact
      parameter(pig=4.d0*datan(1.d0))
      
      cth=dcos(theta)
      call lplm(lmax, cth, lgp)
      
      den=1.d0/dsqrt(4.d0*pig)
      do l=0, lmax
        fac=dsqrt(2.d0*dble(l)+1.d0)*den
        do m=-l, l
          rep=dsqrt(d_fact(l-m)/d_fact(l+m))*lgp(l,m)*fac
          spha(l,m)=rep*dcmplx(dcos(m*phi),dsin(m*phi))
        enddo
      enddo
      
      return
      
      end subroutine spharm
      
      
      subroutine wignerd(mk, theta, phi, d)
      
      implicit none
      
      integer, intent(in) :: mk
      double precision, intent(in) :: theta, phi
      complex*16, intent(out) :: d(3)
      
      integer mp
      
      if ( mk /= 0 ) then
        write(*,*) "Error! wignerd only implemented for linearly polarized light!"
        stop
      endif
      
      do mp=-1, 1
        if ( mp == 0 ) then
            d(mp+2)=dcos(theta)
        else
            d(mp+2)=-mp*dsqrt(0.5d0)*dsin(theta)*dcmplx(dcos(-mp*phi), dsin(-mp*phi))
        endif
      enddo
      
      end
      
      
